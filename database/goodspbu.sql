-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 29, 2020 at 03:24 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goodspbu`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_location`
--

CREATE TABLE `tb_location` (
  `location_id` int(9) NOT NULL,
  `location_name` varchar(250) NOT NULL,
  `location_value` varchar(250) NOT NULL,
  `location_desc` text NOT NULL,
  `location_long` varchar(100) NOT NULL,
  `location_lat` varchar(100) NOT NULL,
  `location_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `location_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_location`
--

INSERT INTO `tb_location` (`location_id`, `location_name`, `location_value`, `location_desc`, `location_long`, `location_lat`, `location_created_at`, `location_updated_at`) VALUES
(1, 'Gudang', 'aaa', 'depan spbu samping ac', '23423234', '545345435', '2020-08-13 03:58:43', '2020-08-13 03:58:43'),
(2, 'a', 'b', 'e', 'c', 'd', '2020-08-13 04:05:26', '2020-08-13 04:05:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_other_product`
--

CREATE TABLE `tb_other_product` (
  `product_id` int(9) NOT NULL,
  `product_code` varchar(150) NOT NULL,
  `product_name` varchar(250) NOT NULL,
  `product_image` varchar(250) NOT NULL,
  `product_date` date NOT NULL,
  `product_value` varchar(250) NOT NULL,
  `product_price_buy` varchar(250) NOT NULL,
  `product_price_sell` varchar(250) NOT NULL,
  `product_price_difference` varchar(250) NOT NULL,
  `unit_id` int(9) NOT NULL,
  `product_description` text NOT NULL,
  `product_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `product_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_other_product`
--

INSERT INTO `tb_other_product` (`product_id`, `product_code`, `product_name`, `product_image`, `product_date`, `product_value`, `product_price_buy`, `product_price_sell`, `product_price_difference`, `unit_id`, `product_description`, `product_created_at`, `product_updated_at`) VALUES
(1, 'B01', 'Oli Astra 300 Ml', 'Screen_Shot_2020-08-05_at_16_47_01.png', '2020-04-28', '', '100', '200', '100', 0, 'okok', '2020-07-31 10:30:13', '2020-08-27 17:46:18'),
(6, 'BR001', 'Bearing bensin', 'gofood.png', '1970-01-01', '', '4000', '6000', '', 0, 'bearing digunakan oleh suplier ', '2020-08-25 09:21:51', '2020-08-27 17:46:20'),
(7, 'q', 'q', 'gofood.png', '2020-07-10', '', '200', 'q', '', 1, 'q', '2020-08-27 17:34:12', '2020-08-27 17:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_other_product_stock`
--

CREATE TABLE `tb_other_product_stock` (
  `stock_id` int(9) NOT NULL,
  `product_id` int(9) NOT NULL,
  `location_id` int(9) NOT NULL,
  `stock_amount` varchar(250) NOT NULL,
  `stock_value` varchar(250) NOT NULL,
  `unit_id` int(9) NOT NULL,
  `stock_desc` text NOT NULL,
  `stock_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `stock_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_price`
--

CREATE TABLE `tb_price` (
  `price_id` int(9) NOT NULL,
  `price_code` varchar(100) NOT NULL,
  `price_discount` varchar(250) NOT NULL,
  `price_buy` varchar(250) NOT NULL,
  `price_sell` varchar(250) NOT NULL,
  `price_difference` varchar(250) NOT NULL,
  `price_value` varchar(250) NOT NULL,
  `price_name` varchar(250) NOT NULL,
  `price_desc` text NOT NULL,
  `price_date` date NOT NULL,
  `product_id` int(9) NOT NULL,
  `status_id` int(9) NOT NULL,
  `price_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `price_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_price`
--

INSERT INTO `tb_price` (`price_id`, `price_code`, `price_discount`, `price_buy`, `price_sell`, `price_difference`, `price_value`, `price_name`, `price_desc`, `price_date`, `product_id`, `status_id`, `price_created_at`, `price_updated_at`) VALUES
(19, '001', '', '8460', '9000', '540', '', '', 'ok', '2020-08-09', 1, 7, '2020-08-09 13:08:10', '2020-08-09 13:08:10'),
(20, '001', '', '6244', '6450', '206', '', '', 'ok', '2020-08-09', 2, 7, '2020-08-09 13:08:10', '2020-08-09 13:08:10'),
(21, '001', '', '7337', '7650', '313', '', '', 'ok', '2020-08-09', 3, 7, '2020-08-09 13:08:10', '2020-08-09 13:08:10'),
(22, '001', '', '', '', '0', '', '', 'ok', '2020-08-09', 4, 7, '2020-08-09 13:08:10', '2020-08-09 13:08:10'),
(23, '001', '', '', '', '0', '', '', 'ok', '2020-08-09', 5, 7, '2020-08-09 13:08:10', '2020-08-09 13:08:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `product_id` int(9) NOT NULL,
  `product_code` varchar(15) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `product_price_buy` varchar(100) NOT NULL,
  `product_price_sell` varchar(100) NOT NULL,
  `product_description` varchar(250) NOT NULL,
  `product_date` datetime NOT NULL,
  `product_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `product_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`product_id`, `product_code`, `product_name`, `product_image`, `product_price_buy`, `product_price_sell`, `product_description`, `product_date`, `product_created_at`, `product_updated_at`) VALUES
(1, 'PTX123', 'Pertamax', 'Screen_Shot_2020-07-06_at_00_08_371.png', '', '', 'this is just a desc', '2020-07-10 00:00:00', '2020-07-05 16:10:46', '2020-07-05 16:14:09'),
(2, 'PRE123', 'Premium', 'Screen_Shot_2020-07-06_at_00_13_082.png', '', '', '', '2020-07-22 00:00:00', '2020-07-07 13:52:35', '2020-07-21 10:56:55'),
(3, 'PER123', 'Pertalite', 'Screen_Shot_2020-07-06_at_00_13_081.png', '', '', 'pertalite desc', '2020-08-01 00:00:00', '2020-07-06 16:12:25', '2020-07-21 10:57:13'),
(4, 'PTB123', 'Pertamax Turbo', 'kosong.jpg', '', '', '', '2020-08-27 00:00:00', '2020-07-21 10:44:44', '2020-08-08 14:28:03'),
(5, 'DXT123', 'Dexlite', 'kosong.jpg', '', '', '', '1970-01-01 00:00:00', '2020-07-21 10:45:00', '2020-07-21 10:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product_price`
--

CREATE TABLE `tb_product_price` (
  `product_price_id` int(9) NOT NULL,
  `product_price_code` varchar(15) NOT NULL,
  `product_id` int(9) NOT NULL,
  `product_price_buy` varchar(100) NOT NULL,
  `product_price_sell` varchar(100) NOT NULL,
  `product_price_date` date NOT NULL,
  `shift_id` int(9) NOT NULL,
  `product_price_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `product_price_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product_price`
--

INSERT INTO `tb_product_price` (`product_price_id`, `product_price_code`, `product_id`, `product_price_buy`, `product_price_sell`, `product_price_date`, `shift_id`, `product_price_created_at`, `product_price_updated_at`) VALUES
(1, '001', 1, '7700', '7900', '2020-07-14', 2, '2020-07-14 07:50:16', '2020-07-14 07:50:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sale`
--

CREATE TABLE `tb_sale` (
  `sale_id` int(9) NOT NULL,
  `shift_id` int(9) NOT NULL,
  `totalisator_id` int(9) NOT NULL,
  `product_id` int(9) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `sale_date` date NOT NULL,
  `totalisator_position_before` varchar(200) NOT NULL,
  `totalisator_position_after` varchar(200) NOT NULL,
  `totalisator_difference` varchar(200) NOT NULL,
  `sale_buy_price` varchar(250) NOT NULL,
  `sale_sell_price` varchar(250) NOT NULL,
  `sale_difference` varchar(250) NOT NULL,
  `sale_value` varchar(250) NOT NULL,
  `sale_info` text NOT NULL,
  `status_id` int(9) NOT NULL,
  `sale_desc` text NOT NULL,
  `sale_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `sale_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sale`
--

INSERT INTO `tb_sale` (`sale_id`, `shift_id`, `totalisator_id`, `product_id`, `user_email`, `sale_date`, `totalisator_position_before`, `totalisator_position_after`, `totalisator_difference`, `sale_buy_price`, `sale_sell_price`, `sale_difference`, `sale_value`, `sale_info`, `status_id`, `sale_desc`, `sale_created_at`, `sale_updated_at`) VALUES
(174, 1, 5, 1, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '2', '2', '8460', '9000', '540', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:49:15'),
(175, 1, 8, 3, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '0', '0', '7337', '7650', '313', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:49:15'),
(176, 1, 9, 3, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '0', '0', '7337', '7650', '313', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:43:03'),
(177, 1, 10, 3, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '0', '0', '7337', '7650', '313', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:43:03'),
(178, 1, 11, 2, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '5', '5', '6244', '6450', '206', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:49:15'),
(179, 1, 12, 2, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '6', '6', '6244', '6450', '206', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:49:15'),
(180, 1, 14, 4, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '0', '0', '', '', '0', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:43:03'),
(181, 1, 15, 5, 'noviadarkbondan@gmail.com', '2020-08-09', '0', '0', '0', '', '', '0', '', '', 5, '', '2020-08-09 13:43:03', '2020-08-09 13:43:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sale_beta`
--

CREATE TABLE `tb_sale_beta` (
  `sale_id` int(9) NOT NULL,
  `shift_id` int(9) NOT NULL,
  `totalisator_id` int(9) NOT NULL,
  `product_id` int(9) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `sale_date` date NOT NULL,
  `totalisator_position` varchar(500) NOT NULL,
  `sale_sell_price` varchar(150) NOT NULL,
  `sale_buy_price` varchar(150) NOT NULL,
  `sale_value` varchar(150) NOT NULL,
  `sale_info` varchar(150) NOT NULL,
  `sale_desc` text NOT NULL,
  `status_id` int(9) NOT NULL,
  `sale_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `sale_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_shift`
--

CREATE TABLE `tb_shift` (
  `shift_id` int(9) NOT NULL,
  `shift_code` varchar(15) NOT NULL,
  `shift_name` varchar(150) NOT NULL,
  `shift_desc` varchar(250) NOT NULL,
  `shift_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `shift_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `shift_start` time NOT NULL,
  `shift_stop` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_shift`
--

INSERT INTO `tb_shift` (`shift_id`, `shift_code`, `shift_name`, `shift_desc`, `shift_created_at`, `shift_updated_at`, `shift_start`, `shift_stop`) VALUES
(1, '001', 'Pagi', 'Dimulai oleh', '2020-07-14 07:50:48', '2020-07-20 09:02:43', '06:00:00', '14:00:00'),
(2, '002', 'Sore', 'shift ini dijaga', '2020-07-19 13:53:01', '2020-07-20 13:15:40', '14:00:00', '22:00:00'),
(3, '003', 'Malam', 'shift ini dijaga', '2020-07-19 14:03:25', '2020-07-20 09:03:23', '10:00:00', '06:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_status`
--

CREATE TABLE `tb_status` (
  `status_id` int(9) NOT NULL,
  `status_name` varchar(50) NOT NULL,
  `status_date` date NOT NULL,
  `status_time` time NOT NULL,
  `status_value` text NOT NULL,
  `status_for` varchar(150) NOT NULL,
  `status_desc` text NOT NULL,
  `status_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `status_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_status`
--

INSERT INTO `tb_status` (`status_id`, `status_name`, `status_date`, `status_time`, `status_value`, `status_for`, `status_desc`, `status_created_at`, `status_updated_at`) VALUES
(1, 'Aktif', '2020-07-01', '33:32:00', 'value', 'totalisator', 'staus descript', '2020-07-18 06:22:36', '2020-07-18 16:16:27'),
(2, 'Nonaktif', '0000-00-00', '00:00:00', 'value', 'totalisator', 'ok', '2020-07-18 14:37:57', '2020-07-20 08:31:55'),
(3, 'Biasa', '0000-00-00', '00:00:00', 'val biasa', 'user', 'desc\r\n', '2020-07-18 14:39:34', '2020-07-18 14:39:34'),
(5, 'Active', '0000-00-00', '00:00:00', '', 'sale', '', '2020-07-19 15:09:01', '2020-07-19 15:09:43'),
(6, 'Inactive', '0000-00-00', '00:00:00', '', 'sale', '', '2020-07-19 15:09:13', '2020-07-19 15:09:58'),
(7, 'Aktif', '0000-00-00', '00:00:00', '', 'price', '', '2020-07-20 08:31:35', '2020-07-20 08:31:35'),
(8, 'Nonaktif', '0000-00-00', '00:00:00', '', 'price', '', '2020-07-20 08:31:43', '2020-07-20 08:31:43'),
(9, 'Admin', '0000-00-00', '00:00:00', '', 'user', '', '2020-07-20 08:32:06', '2020-07-20 08:32:06'),
(10, 'lolok', '0000-00-00', '00:00:00', '', 'totalisator', '', '2020-07-30 12:01:26', '2020-07-30 12:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_totalisator`
--

CREATE TABLE `tb_totalisator` (
  `totalisator_id` int(9) NOT NULL,
  `totalisator_code` varchar(15) NOT NULL,
  `totalisator_name` varchar(100) NOT NULL,
  `product_id` int(9) NOT NULL,
  `totalisator_current_position` varchar(100) NOT NULL,
  `status_id` int(9) NOT NULL,
  `totalisator_desc` text NOT NULL,
  `totalisator_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `totalisator_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_totalisator`
--

INSERT INTO `tb_totalisator` (`totalisator_id`, `totalisator_code`, `totalisator_name`, `product_id`, `totalisator_current_position`, `status_id`, `totalisator_desc`, `totalisator_created_at`, `totalisator_updated_at`) VALUES
(5, '01', 'Totalisator 01 Pertamax', 1, '', 1, 'ok', '2020-07-19 14:23:35', '2020-07-19 14:31:32'),
(8, '02', 'Totalisator 01 Pertalite', 3, '', 1, '', '2020-07-19 14:28:33', '2020-07-23 15:35:36'),
(9, '03', 'Totalisator 02 Pertalite', 3, '', 1, '', '2020-07-19 14:30:42', '2020-07-23 15:35:39'),
(10, '04', 'Totalisator 03 Pertalite', 3, '', 1, '', '2020-07-19 14:31:51', '2020-07-23 15:35:42'),
(11, '05', 'Totalisator 01 Premium', 2, '', 1, '', '2020-07-19 14:32:08', '2020-07-23 15:35:45'),
(12, '06', 'Totalisator 02 Premium', 2, '', 1, '', '2020-07-19 14:32:34', '2020-07-23 15:35:49'),
(14, '07', 'Totalisator 1 Pertamax Turbo', 4, '', 1, '', '2020-07-21 10:44:09', '2020-07-23 15:35:52'),
(15, '08', 'Totalisator 01 Dexlite', 5, '', 1, '', '2020-07-21 10:48:36', '2020-07-23 15:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_unit`
--

CREATE TABLE `tb_unit` (
  `unit_id` int(9) NOT NULL,
  `unit_name` varchar(100) NOT NULL,
  `unit_value` varchar(250) NOT NULL,
  `unit_desc` text NOT NULL,
  `unit_created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `unit_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_unit`
--

INSERT INTO `tb_unit` (`unit_id`, `unit_name`, `unit_value`, `unit_desc`, `unit_created_at`, `unit_updated_at`) VALUES
(0, 'Kg', 'Kilogram', 'okok', '2020-08-11 01:06:44', '2020-08-27 17:46:43'),
(1, 'M', 'Meter', 'unit Desc', '2020-08-11 00:59:36', '2020-08-11 01:12:28'),
(4, 'Pcs', 'Pieces', 'okok', '2020-08-11 01:07:00', '2020-08-11 01:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `user_id` int(9) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_first_name` varchar(100) NOT NULL,
  `user_last_name` varchar(100) NOT NULL,
  `user_password` varchar(300) NOT NULL,
  `user_address` text NOT NULL,
  `user_profile_pict` varchar(300) NOT NULL,
  `user_dob` date NOT NULL,
  `user_pob` varchar(100) NOT NULL,
  `user_desc` text NOT NULL,
  `user_status` int(9) NOT NULL,
  `user_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `user_name`, `user_email`, `user_first_name`, `user_last_name`, `user_password`, `user_address`, `user_profile_pict`, `user_dob`, `user_pob`, `user_desc`, `user_status`, `user_created_at`, `user_updated_at`) VALUES
(2, 'bondan', 'noviadarkbondan@gmail.com', '', '', '5b9342138fb2877bace4eea012699218', '', 'bondan.png', '1970-01-01', '', 'bondan', 1, '2020-02-09 01:25:24', '2020-02-20 06:45:17'),
(14, 'Astra Danta', 'astra.danta@gmail.com', '', '', '5f4dcc3b5aa765d61d8327deb882cf99', '', '4.png', '1970-05-06', '', 'Astra Danta', 2, '2020-03-25 00:34:48', '2020-07-12 13:45:46'),
(16, 'bondan', 'gud@goodponsel.com', '', '', '4252006395f926ae0346a51b9483779c', '', '3.png', '2020-02-18', '', 'okok', 2, '2020-07-12 14:07:23', '2020-07-12 14:07:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_location`
--
ALTER TABLE `tb_location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `tb_other_product`
--
ALTER TABLE `tb_other_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tb_other_product_stock`
--
ALTER TABLE `tb_other_product_stock`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `tb_price`
--
ALTER TABLE `tb_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tb_product_price`
--
ALTER TABLE `tb_product_price`
  ADD PRIMARY KEY (`product_price_id`);

--
-- Indexes for table `tb_sale`
--
ALTER TABLE `tb_sale`
  ADD PRIMARY KEY (`sale_id`);

--
-- Indexes for table `tb_sale_beta`
--
ALTER TABLE `tb_sale_beta`
  ADD PRIMARY KEY (`sale_id`);

--
-- Indexes for table `tb_shift`
--
ALTER TABLE `tb_shift`
  ADD PRIMARY KEY (`shift_id`);

--
-- Indexes for table `tb_status`
--
ALTER TABLE `tb_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tb_totalisator`
--
ALTER TABLE `tb_totalisator`
  ADD PRIMARY KEY (`totalisator_id`);

--
-- Indexes for table `tb_unit`
--
ALTER TABLE `tb_unit`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_location`
--
ALTER TABLE `tb_location`
  MODIFY `location_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_other_product`
--
ALTER TABLE `tb_other_product`
  MODIFY `product_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_other_product_stock`
--
ALTER TABLE `tb_other_product_stock`
  MODIFY `stock_id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_price`
--
ALTER TABLE `tb_price`
  MODIFY `price_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `product_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_product_price`
--
ALTER TABLE `tb_product_price`
  MODIFY `product_price_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_sale`
--
ALTER TABLE `tb_sale`
  MODIFY `sale_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `tb_sale_beta`
--
ALTER TABLE `tb_sale_beta`
  MODIFY `sale_id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_shift`
--
ALTER TABLE `tb_shift`
  MODIFY `shift_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_status`
--
ALTER TABLE `tb_status`
  MODIFY `status_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_totalisator`
--
ALTER TABLE `tb_totalisator`
  MODIFY `totalisator_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tb_unit`
--
ALTER TABLE `tb_unit`
  MODIFY `unit_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
