<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content card white">
						<h4 class="box-title">Add a Sale</h4>
						<!-- /.box-title -->
						<div class="card-content">
                            <?php echo form_open_multipart('sales/edit_process','class="form" id="product_add_form"');?>  
								<div class="form-group">
									<label>Sales Date (Bulan/Tanggal/Tahun)</label>
									<div class="input-group col-xs-4">
										<input type="text" name="sale_id" id="sale_id" hidden id="inp-type-1" placeholder="Product" value="<?php echo $sale['sale_id'];?>">	
										<input type="text" name="sale_date" id="datepicker" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" value="<?php echo date("m/d/Y", strtotime($sale['sale_date']));?>">
										<span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
									</div>
                                </div>
                                <div class="form-group">
									<label>Shift</label>
									<select class="form-control" name="shift_id" id="shift_id" >
											<option value="<?php echo $sale['shift_id']?>"><?php echo $shift_name?></option>
										<?php foreach ($shifts as $shift) :  ?>
										    <option value="<?php echo $shift['shift_id']?>"><?php echo $shift['shift_name']?></option>
										<?php endforeach; ?>
									</select>
                                </div>  
                                <div class="form-group">
									<label>Totalisator</label>
                                    <select class="form-control" name="totalisator_id" id="totalisator_id" onchange="getProduct()">
									<option value="<?php echo $sale['totalisator_id']?>"><?php echo $totalisator_name."-".$sale['product_id']?></option>
                                        <?php $i=1; foreach ($totalisators as $totalisator) :  ?>
                                            <?php 
                                                $string_i = strval($i);
                                                $product_id = $totalisator['product_id'];
                                                $i=$i+1;
                                            ?>
										    <option value="<?php echo $totalisator['totalisator_id']?>"><?php echo $totalisator['totalisator_name']."-".$product_id?></option>
										<?php endforeach; ?>
                                    </select>
                                    <script>
                                        function getProduct() {
                                        var x = document.getElementById("totalisator_id").value;
                                        var text_selection = document.getElementById("totalisator_id").options[document.getElementById("totalisator_id").selectedIndex ].text
                                        //substring from the last
                                        var product_id = text_selection.substr(text_selection.length - 1); // => "1"
                                        document.getElementById("product_id").value = product_id;
                                        //AJAX request
											$.ajax({
												url: '<?php echo base_url();?>product/get_product_name/'+product_id,
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													// $('.modal-body<?php //echo $totalisator['totalisator_id'];?>').html(response); 

													// // Display Modal
                                                    // $('#boostrapModal-<?php //echo $totalisator['totalisator_id'];?>').modal('show'); 
                                                    
                                                    document.getElementById("product_name").value =  response;
												}
											});

											$.ajax({
												url: '<?php echo base_url();?>price/get_active_price/'+product_id,
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													// $('.modal-body<?php //echo $totalisator['totalisator_id'];?>').html(response); 

													// // Display Modal
													// $('#boostrapModal-<?php //echo $totalisator['totalisator_id'];?>').modal('show'); 
													var obj = JSON.parse(response);
													document.getElementById("sale_sell_price").value =  obj.price_sell;
													document.getElementById("sale_buy_price").value =  obj.price_buy;
													document.getElementById("price_used").value =  obj.price_sell;
													document.getElementById("sale_difference_price").value =  obj.price_difference;
												}
											});
										
                                        }
                                    </script>
                                </div>  
                                <div class="form-group">
                                    <label>Product</label>
                                    <p id="totalisator_id_result"></p>
                                    <input type="text" name="product_id" id="product_id" hidden  id="inp-type-1" placeholder="Product" value="<?php echo $sale['product_id'];?>">
									<input type="text" name="product_name" disabled id="product_name" class="form-control" id="inp-type-1" placeholder="Product" value="<?php echo $product_name;?>">
                                </div>
                                <div class="form-group">
                                    <label>Price Used</label>
                                    <p id="totalisator_id_result"></p>
									<input type="text" name="sale_buy_price" hidden id="sale_buy_price"  id="inp-type-1" placeholder="Price" value="<?php echo $sale['sale_buy_price'];?>">
									<input type="text" name="sale_sell_price" hidden id="sale_sell_price"  id="inp-type-1" placeholder="Price" value="<?php echo $sale['sale_sell_price'];?>">
									<input type="text" name="sale_difference" hidden id="sale_difference_price"  id="inp-type-1" placeholder="Price" value="<?php echo $sale['sale_difference'];?>">
									<input type="text" name="price_used" disabled id="price_used" class="form-control" id="inp-type-1" placeholder="Price" value="<?php echo $sale['sale_sell_price'];?>">
								</div>
								<div class="form-group">
									<label>User</label>
									<input type="text" name="user_email"  hidden id="user_email" id="inp-type-1" placeholder="Enter product code" value="<?php echo $user_email?>">
									<input type="text" name="user_email_disabled"  disabled id="user_email_disabled" class="form-control" id="inp-type-1" placeholder="Enter product code" value="<?php echo $user_email?>">
								</div>
								<div class="form-group">
									<label>Totalisator Before (angka desimal, pakai "." bukan ",") pakai titik, bukan koma</label>
									<input type="text" name="totalisator_position_before" id="totalisator_position_before" class="form-control" id="inp-type-1" placeholder="Enter totalisator value before" onchange="calculateDifference()" value="<?php echo $sale['totalisator_position_before'];?>">
                                </div>
                                <div class="form-group">
									<label>Totalisator After (angka desimal, pakai "." bukan ",") pakai titi, bukan koma</label>
									<input type="text" name="totalisator_position_after" id="totalisator_position_after" class="form-control" id="inp-type-1" placeholder="Enter totalisator value before" onchange="calculateDifference()" value="<?php echo $sale['totalisator_position_after'];?>">
                                </div>
                                <script>
                                    function calculateDifference() {
                                    var before = parseFloat(document.getElementById("totalisator_position_before").value); 
                                    var after = parseFloat(document.getElementById("totalisator_position_after").value); 
                                    var difference = after - before;
                                    document.getElementById("totalisator_difference").value = difference;
                                    }
                                </script>
                                <div class="form-group">
									<label>Totalisator Difference (click me)</label>
									<input type="text" name="totalisator_difference" disabled id="totalisator_difference" class="form-control" id="inp-type-1" placeholder="Enter totalisator value before" value="<?php echo $sale['totalisator_difference'];?>">
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status_id" id="status_id" >
										<option value="<?php echo $sale['status_id']?>"><?php echo $status_name?></option>
										<?php foreach ($statuses as $status) :  ?>
										<option value="<?php echo $status['status_id']?>"><?php echo $status['status_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Sale Note</label>
									<textarea class="form-control" name="sale_desc" id="sale_desc" id="inp-type-5" placeholder="Write product description"><?php echo $sale['sale_desc'];?></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Update</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	

