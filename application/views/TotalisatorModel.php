<?php
class TotalisatorModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_product($product_id = FALSE){
        if($product_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_product");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_product WHERE product_id =".$product_id);
        return $query->row_array();
  }

  public function add_product($add_data){
    $product_image = $add_data['file_name'];

    //changing date format to phpmyadmin
    $orgDate = $this->input->post('product_date');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $dob = $newDate;

		$data = array(
      'product_code' => $this->input->post('product_code'),
      'product_name' => $this->input->post('product_name'),
			'product_image' => $product_image,
      'product_date' => $dob,
      'product_description' => $this->input->post('product_desc')
		);
		return $this->db->insert('tb_product',$data);
  }
  
  public function update_product($additional_data = FALSE)
  {
    $product_id = $this->input->post('product_id');
    $product_code = $this->input->post('product_code');
    $product_name = $this->input->post('product_name');
    $product_description = $this->input->post('product_description');

    //changing date format to phpmyadmin database
    $orgDate = $this->input->post('product_date');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $product_date = $newDate;
   
    
   if(isset($additional_data['file_name'])){
     $data  = array(
       'product_code'          => $product_code,
       'product_name'         => $product_name,
       'product_image'  => $additional_data['file_name'],
       'product_date'           => $product_date,
       'product_description'          => $product_description,
    );
  } else {
    $data  = array(
      'product_code'          => $product_code,
       'product_name'         => $product_name,
       'product_date'           => $product_date,
       'product_description'          => $product_description,
   );
  }
   $this->db->where('product_id', $product_id);
   return $this->db->update('tb_product', $data);
  }

  public function product_delete($id){
    $this->db->where('product_id', $id);
    return $this->db->delete('tb_product');
  }


  //mengecek productname dengan num_row
  public function get_num_product_rows($data){
    $email = $data['product_email'];
    $password = md5($data['product_password']);

    $query = $this->db->query("SELECT * FROM tb_product where product_email = '$email' and product_password = '$password'");
    return $query->num_rows();
  }

}
