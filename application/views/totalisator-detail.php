				<div class="box-content">
					<table class="table table-striped">
						<tbody> 
							<tr> 
								<th scope="row">Product Code</th> 
								<td><?php echo $product['product_code'];?></td>
							</tr> 
							<tr> 
								<th scope="row">Product Picture</th> 
								<td><img class="form-control" id="uploadPreview" style="width: 136px; height: 76px;" src="<?php echo base_url();?>assets/img/product/<?php echo $product['product_image']?>"/></td>
							</tr> 
							<tr> 
								<th scope="row">Product Name</th> 
								<td><?php echo $product['product_name'];?></td>
							<tr> 
							<tr> 
								<th scope="row">Product Description</th> 
								<td><?php echo $product['product_description'];?></td>
							<tr> 
								<th scope="row">Product Created At</th> 
								<td><?php echo $product['product_created_at'];?></td>
							</tr> 
							<tr> 
								<th scope="row">Product Updated At</th> 
								<td><?php echo $product['product_updated_at'];?></td>
							</tr> 
						</tbody> 
					</table>
					<p><strong class="text-info">Note:</strong> Please wait for the data to be finalized!</p>
				</div>  