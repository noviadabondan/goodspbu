

<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add a Shift</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('shift/add_process','class="form" id="user_add_form"');?>    
								<div class="form-group">
									<label>Shift Code</label>
									<input type="text" name="shift_code" id="shift_code" class="form-control" id="inp-type-1" placeholder="Enter your Name">
								</div>
								<div class="form-group">
									<label>Shift Name</label>
									<input type="text" name="shift_name" id="shift_name" class="form-control" id="inp-type-1" placeholder="Enter your Name">
								</div>
								<div class="form-group">
									<label>Shift Start</label>
									<div class="input-group margin-bottom-20 col-xs-4">
									<div class="bootstrap-timepicker">
										<input id="timepicker2" name="shift_start" type="text" class="form-control">
									</div>
									<span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
								</div>
								<div class="form-group">
									<label>Shift Stop</label>
									<div class="input-group margin-bottom-20 col-xs-4">
									<div class="bootstrap-timepicker">
										<input id="timepicker2" name="shift_stop" type="text" class="form-control">
									</div>
									<span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
								</div>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea class="form-control" name="shift_desc" id="shift_desc" id="inp-type-5" placeholder="Write your meassage"></textarea>
								</div>
								
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	
