<?php 
	$id = "";
?>

<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
            <div class="box-content">
			<a href="<?php echo base_url();?>stock/other_product_add/<?php echo $product_id;?>" type="button" class="btn btn-success waves-effect waves-light">+ Add Data</a>
            </div>
            </div>
            

            <div class="col-xs-12">
				<div class="box-content">
                    <h4 class="box-title">Stock <?php echo $product_name?> ------- <a href="<?php echo base_url();?>product/other_product">Back to Product List</a></h4>
					<!-- /.dropdown js__dropdown -->
					<table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
							<tr>
								<th>Location</th>
								<th>Stock/Unit</th>
								<th>Tanggal Input</th>
								<th>Desc</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>

                            <?php $stock_amount = 0; foreach ($stocks as $stock) :  ?>
							<tr>
                                <td><?php echo $stock['location_name'];?></td>
                                <td><?php echo $stock['stock_amount'].' '.$stock['unit_name'];?></td>
								<td><?php echo $stock['stock_created_at'];?></td>
								<td><?php echo $stock['stock_desc'];?></td>
                                <td>
									<a type="button" href="<?php echo base_url();?>stock/edit/<?php echo $stock['stock_id']; ?>" class="btn btn-primary btn-circle btn-xs waves-effect waves-light"><i class="ico fa fa-edit"></i></a>
                                </td>
                            </tr>
                            <?php $stock_amount = $stock_amount +  $stock['stock_amount']; $unit_name = $stock['unit_name']; endforeach;?>
						</tbody>
						<tfoot>
							<tr>
								<th>Location</th>
								<th>Total : <?php echo $stock_amount.' '; 
								if(isset($unit_name)){ 
									echo $unit_name; 
									} else { 
										echo "";  };?></th>
								<th>Tanggal Input</th>
								<th>Desc</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
					<script>
						function changeValue(str) {
							var x = 1;
						}
						function deleteproduct(x) {
						var xhttp;
						if (x.length == 0) { 
							document.getElementById("txtHint").innerHTML = "";
							return;
						}
						xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
							document.getElementById("txtHint").innerHTML = this.responseText;
							location.reload();
							}
						};
						xhttp.open("GET", "product/delete/"+x, true);
						xhttp.send();   
						}
					</script>
				<!-- /.box-content -->
			</div>
        </div>
	</div>
</div>

<!-- Modal Detail -->
<?php foreach ($stocks as $stock) :  ?>
<div class="modal fade" id="boostrapModal-<?php echo $stock['stock_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">User Detail</h4>
			</div>
			<div class="modal-body<?php echo $stock['stock_id'];?>">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm waves-effect waves-light" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<?php foreach ($stocks as $stock) :  ?>
<div class="modal fade" id="boostrapModalDelete-<?php echo $stock['stock_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">Delete Data</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin untuk menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm waves-effect waves-light" data-dismiss="modal">No</button>
				<a type="button" class="btn btn-danger btn-sm waves-effect waves-light" href="<?php echo base_url();?>stock/delete/<?php echo $stock['stock_id'];?>" >Yes</a>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>