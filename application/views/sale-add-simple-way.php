<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content card white">
						<h4 class="box-title">Add a Sale in Simple Way</h4>
						<!-- /.box-title -->
						<div class="card-content">
                            <?php echo form_open_multipart('sales/add_process','class="form" id="product_add_form"');?>  
								<div class="form-group">
									<label>Sales Date (Bulan/Tanggal/Tahun)</label>
									<div class="input-group col-xs-4">
										<input type="text" name="sale_date" id="datepicker" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" value="<?php echo date("m/d/yy");?>">
										<span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
									</div>
                                </div>
                                <div class="form-group">
									<label>Shift</label>
									<select class="form-control" name="shift_id" id="shift_id" onchange="getTotalisator()" >
										<?php foreach ($shifts as $shift) :  ?>
										    <option value="<?php echo $shift['shift_id']?>"><?php echo $shift['shift_name']?></option>
										<?php endforeach; ?>
									</select>
                                </div>
								<div class="form-group">
									<label>Product</label>
									<select class="form-control" name="product_id" id="product_id" onchange="getTotalisator()">
                                        <option value="">nothing is selected</option>
										<?php foreach ($products as $product) :  ?>
										    <option value="<?php echo $product['product_id']?>"><?php echo $product['product_name']?></option>
										<?php endforeach; ?>
									</select>
                                </div>
								<h4 class="product-price">
								</h4>
								<div class="totalisator-input">
								</div>
									<script>
                                        function getTotalisator() {
                                        var date = document.getElementById("datepicker").value;
										var shift_id = document.getElementById("shift_id").value;
										var product_id = document.getElementById("product_id").value;
                                        //var text_selection = document.getElementById("totalisator_id").options[document.getElementById("totalisator_id").selectedIndex ].text
                                        //substring from the last
                                        //var product_id = text_selection.substr(text_selection.length - 1); // => "1"
                                        //document.getElementById("product_id").value = product_id;
                                        //AJAX request
											$.ajax({
												url: '<?php echo base_url();?>totalisator/get_totalisator_input_by_product/'+product_id,
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													// $('.modal-body<?php //echo $totalisator['totalisator_id'];?>').html(response); 
													// // Display Modal
                                                    // $('#boostrapModal-<?php //echo $totalisator['totalisator_id'];?>').modal('show'); 
                                                    $('.totalisator-input').html(response);
												}
											});
											$.ajax({
												url: '<?php echo base_url();?>price/get_price/'+product_id,
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													// $('.modal-body<?php //echo $totalisator['totalisator_id'];?>').html(response); 
													// // Display Modal
                                                    // $('#boostrapModal-<?php //echo $totalisator['totalisator_id'];?>').modal('show'); 
                                                    $('.product-price').html("Price Used : IDR "+response);
												}
											});
                                        }
                                    </script>
								
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	

