<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

    <title>Home</title>
    <!-- Main Styles -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/styles/style-horizontal.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/styles/style.min.css">

	<!-- Waves Effect -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/waves/waves.min.css">

</head>

<body>

<div id="single-wrapper">
    <?php echo form_open_multipart('userAuth/login_process','role="form" class="frm-single"');?>
		<div class="inside">
			<div class="title"><strong>Good</strong>SPBU</div>
			<!-- /.title -->
			<div class="frm-title">
			<?php 
			//$error = "<br>eror";
			if(isset($error)){
				echo $error;
			} else {

			}
			
			
			?>
			
			
			</div>
			<!-- /.frm-title -->
			<div class="frm-input"><input name="user_email" id="user_email" type="email" placeholder="Username" class="frm-inp"><i class="fa fa-user frm-ico"></i></div>
			<!-- /.frm-input -->
			<div class="frm-input"><input name="user_password" id="user_password" type="password" placeholder="Password" class="frm-inp"><i class="fa fa-lock frm-ico"></i></div>
			<!-- /.frm-input -->
			<div class="clearfix margin-bottom-20">
				<div class="pull-left">
					<div class="checkbox primary"><input type="checkbox" id="rememberme"><label for="rememberme">Remember me</label></div>
					<!-- /.checkbox -->
				</div>
				<!-- /.pull-left -->
				<div class="pull-right"><a href="page-recoverpw.html" class="a-link"><i class="fa fa-unlock-alt"></i>Forgot password?</a></div>
				<!-- /.pull-right -->
			</div>
			<!-- /.clearfix -->
			<button type="submit" class="frm-submit">Login<i class="fa fa-arrow-circle-right"></i></button>
			<div class="row small-spacing">
				
			</div>
			<div class="frm-footer">ExacTech 2020.</div>
			<!-- /.footer -->
		</div>
		<!-- .inside -->
	</form>
	<!-- /.frm-single -->
</div><!--/#single-wrapper -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="assets/script/html5shiv.min.js"></script>
		<script src="assets/script/respond.min.js"></script>
	<![endif]-->
	<!-- 
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/scripts/jquery.min.js"></script>
	<script src="assets/scripts/modernizr.min.js"></script>
	<script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/plugin/nprogress/nprogress.js"></script>
	<script src="assets/plugin/waves/waves.min.js"></script>

	<script src="assets/scripts/main.min.js"></script>
</body>
</html>