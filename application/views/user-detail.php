				<div class="box-content">
					<table class="table table-striped">
						<tbody> 
							<tr> 
								<th scope="row">User Code</th> 
								<td><?php echo $user['user_id'];?></td>
							</tr> 
							<tr> 
								<th scope="row">User Picture</th> 
								<td><img class="form-control" id="uploadPreview" style="width: 136px; height: 76px;" src="<?php echo base_url();?>assets/img/user/<?php echo $user['user_profile_pict']?>"/></td>
							</tr> 
							<tr> 
								<th scope="row">User Name</th> 
								<td><?php echo $user['user_name'];?></td>
							<tr> 
							<tr> 
								<th scope="row">User Date of Birth</th> 
								<td><?php echo $user['user_dob'];?></td>
							<tr> 
							<tr> 
								<th scope="row">User Status</th> 
								<td><?php echo $user['user_status'];?></td>
							<tr> 
							<tr> 
								<th scope="row">User Description</th> 
								<td><?php echo $user['user_desc'];?></td>
							<tr> 
								<th scope="row">User Created At</th> 
								<td><?php echo $user['user_created_at'];?></td>
							</tr> 
							<tr> 
								<th scope="row">User Updated At</th> 
								<td><?php echo $user['user_updated_at'];?></td>
							</tr> 
						</tbody> 
					</table>
					<p><strong class="text-info">Note:</strong> Please wait for the data to be finalized!</p>
				</div>  