<?php 
	$id = "";
?>

<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
            <div class="box-content">
			<a href="<?php echo base_url();?>location/add" type="button" class="btn btn-success waves-effect waves-light">+ Add Data</a>
            </div>
            </div>
            

            <div class="col-xs-12">
				<div class="box-content">
                    <h4 class="box-title">location Data</h4>
                    
					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						<ul class="sub-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else there</a></li>
							<li class="split"></li>
							<li><a href="#">Separated link</a></li>
						</ul>
						<!-- /.sub-menu -->
					</div>
					<!-- /.dropdown js__dropdown -->
					<table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
							<tr>
                                <th>ID</th>
								<th>Name</th>
								<th>Value</th>
                                <th>Description</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
                                <th>ID</th>
								<th>Name</th>
								<th>Value</th>
                                <th>Description</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
                            <?php foreach ($locations as $location) :  ?>
							<tr>
                                <td><?php echo $location['location_id'];?></td>
                                <td><?php echo $location['location_name'];?></td>
                                <td><?php echo $location['location_value'];?></td>
                                <td><?php echo $location['location_desc'];?></td>
                                <td>
									<a type="button" href="<?php echo base_url();?>location/edit/<?php echo $location['location_id']; ?>" class="btn btn-primary btn-circle btn-xs waves-effect waves-light"><i class="ico fa fa-edit"></i></a>
									<button type="button" class="btn btn-danger btn-circle btn-xs waves-effect waves-light" data-toggle="modal" onclick="deleteData(<?php echo $location['location_id']; ?>)" data-target="#boostrapModalDelete-<?php echo $location['location_id'];?>"><i class="ico fa fa-trash"></i></button>
                                </td>
                            </tr>
                            <?php endforeach;?>
						</tbody>
					</table>
				</div>
					<script>
						function deletData(str) {
							var x = 1;
						}
						function deleteproduct(x) {
						var xhttp;
						if (x.length == 0) { 
							document.getElementById("txtHint").innerHTML = "";
							return;
						}
						xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.location == 200) {
							document.getElementById("txtHint").innerHTML = this.responseText;
							location.reload();
							}
						};
						xhttp.open("GET", "product/delete/"+x, true);
						xhttp.send();   
						}
					</script>
				<!-- /.box-content -->
			</div>
        </div>
	</div>
</div>


<!-- Modal Delete -->
<?php foreach ($locations as $location) :  ?>
<div class="modal fade" id="boostrapModalDelete-<?php echo $location['location_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">Delete Data</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin untuk menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm waves-effect waves-light" data-dismiss="modal">No</button>
				<a type="button" class="btn btn-danger btn-sm waves-effect waves-light" href="<?php echo base_url();?>location/delete/<?php echo $location['location_id'];?>" >Yes</a>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>