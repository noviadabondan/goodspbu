
<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add a price Data</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('price/add_process_by_product','class="form" id="totalisator_add_form"');?>    
								<div class="form-group">
									<label>Price Date (Bulan/Tanggal/Tahun)</label>
									<div class="input-group col-xs-4">
										<input type="text" name="price_date" id="datepicker" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" value="<?php echo date("m/d/yy");?>">
										<span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
									</div>
                                </div>
								<div class="form-group">
									<label>Price Code</label>
									<input type="text" name="price_code" id="price_code" class="form-control" id="inp-type-1" placeholder="Enter price code">
								</div>
								<?php foreach ($products as $product) :  ?>
								<div class="form-group">
									<label>Harga <?php echo $product['product_name'];?></label>
									<input type="number" name="price_buy_<?php echo $product['product_id'];?>" id="price_buy_<?php echo $product['product_id'];?>" class="form-control" id="inp-type-1" placeholder="Enter buying price for <?php echo $product['product_name'];?>" onchange="calculateDifference<?php echo $product['product_id'];?>()">
									<input type="number" name="price_sell_<?php echo $product['product_id'];?>" id="price_sell_<?php echo $product['product_id'];?>" class="form-control" id="inp-type-1" placeholder="Enter selling price for <?php echo $product['product_name'];?>" onchange="calculateDifference<?php echo $product['product_id'];?>()">
									<input type="number" name="price_difference_<?php echo $product['product_id'];?>" id="price_difference_<?php echo $product['product_id'];?>" disabled class="form-control" id="inp-type-1" placeholder="Enter selling price for <?php echo $product['product_name'];?>">
									<script>
										function calculateDifference<?php echo $product['product_id'];?>() {
										var before = document.getElementById("price_buy_<?php echo $product['product_id'];?>").value; 
										var after = document.getElementById("price_sell_<?php echo $product['product_id'];?>").value; 
										var difference = after - before;
										document.getElementById("price_difference_<?php echo $product['product_id'];?>").value = difference;
										}
									</script>
                                </div>
								<?php endforeach; ?>
                                
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status_id" id="status_id" >
										<?php foreach ($statuses as $status) :  ?>
										<option value="<?php echo $status['status_id']?>"><?php echo $status['status_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Price Description</label>
									<textarea class="form-control" name="price_desc" id="price_desc" id="inp-type-5" placeholder="Write totalisator description"></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	