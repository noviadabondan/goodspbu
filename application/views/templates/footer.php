	
		<footer class="footer">
			<ul class="list-inline">
				<li>2020 © Good SPBU System.</li>
				<li><a href="#">Privacy</a></li>
				<li><a href="#">Terms</a></li>
				<li><a href="#">Help</a></li>
			</ul>
		</footer>
	</div>
	<!-- /.main-content -->
</div><!--/#wrapper -->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="assets/script/html5shiv.min.js"></script>
		<script src="assets/script/respond.min.js"></script>
	<![endif]-->
	<!-- 
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/jquery.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/modernizr.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/nprogress/nprogress.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/sweet-alert/sweetalert.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/waves/waves.min.js"></script>
	<!-- Full Screen Plugin -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/fullscreen/jquery.fullscreen-min.js"></script>

	<!-- Flex Datalist -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/flexdatalist/jquery.flexdatalist.min.js"></script>

	<!-- Popover -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/popover/jquery.popSelect.min.js"></script>

	<!-- Select2 -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/select2/js/select2.min.js"></script>

	<!-- Multi Select -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/multiselect/multiselect.min.js"></script>

	<!-- Percent Circle -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/percircle/js/percircle.js"></script>

	<!-- Touch Spin -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/touchspin/jquery.bootstrap-touchspin.min.js"></script>

	<!-- Google Chart -->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<!-- Chartist Chart -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/chart/chartist/chartist.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/chart.chartist.init.min.js"></script>

	<!-- FullCalendar -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/moment/moment.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/fullcalendar/fullcalendar.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/fullcalendar.init.js"></script>

	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/main.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/horizontal-menu.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/color-switcher/color-switcher.min.js"></script>

	<!-- Data Tables -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/datatables/extensions/Responsive/js/responsive.bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/datatables.demo.min.js"></script>

	<!-- Datepicker -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
	
	<!-- Timepicker -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/timepicker/bootstrap-timepicker.min.js"></script>
	
	<!-- Demo Scripts -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/form.demo.min.js"></script>

	<!-- Remodal -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/modal/remodal/remodal.min.js"></script>

	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/main.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/scripts/horizontal-menu.min.js"></script>
	<script src="<?php echo base_url()?>assets/backoffice/assets/color-switcher/color-switcher.min.js"></script>

	 <!-- Maxlength -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/plugin/maxlength/bootstrap-maxlength.min.js"></script>

</body>
</html>