<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>goodspbu</title>

	<!-- Main Styles -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/styles/style-horizontal.min.css">

	<!-- mCustomScrollbar -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

	<!-- Waves Effect -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/waves/waves.min.css">

	<!-- Sweet Alert -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/sweet-alert/sweetalert.css">
	
	<!-- Percent Circle -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/percircle/css/percircle.css">

	<!-- Chartist Chart -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/chart/chartist/chartist.min.css">

	<!-- FullCalendar -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/fullcalendar/fullcalendar.print.css" media='print'>

	<!-- Datepicker -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/datepicker/css/bootstrap-datepicker.min.css">

	<!-- Color Picker -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/color-switcher/color-switcher.min.css">
	
	<!-- Timepicker -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/timepicker/bootstrap-timepicker.min.css">
	
	<!-- Data Tables -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/datatables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css">

	<!-- Remodal -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/modal/remodal/remodal.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/backoffice/assets/plugin/modal/remodal/remodal-default-theme.css">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/backoffice/assets/plugin/select2/css/select2.min.css">
	
	<!-- JQuery -->
	<script src='<?php echo base_url()?>assets/backoffice/assets/jquery-3.1.1.min.js' type='text/javascript'></script>

	<!-- CKEditor -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/ckeditor/ckeditor.js"></script>

	<!-- TinyMce -->
	<script src="<?php echo base_url()?>assets/backoffice/assets/tinymce2/tinymce.min.js"></script>

	

</head>

<body>
<header class="fixed-header">
	<div class="header-top">
		<div class="container">
			<div class="pull-left">
				<a href="index.html" class="logo">Good SPBU System</a>
			</div>
			<!-- /.pull-left -->
			<div class="pull-right">
				<!-- /.ico-item -->
				<div class="ico-item fa fa-arrows-alt js__full_screen"></div>
				<!-- /.ico-item fa fa-fa-arrows-alt -->
				<div class="ico-item">
					<a href="#" class="ico-item fa fa-bell notice-alarm js__toggle_open" data-target="#notification-popup"></a>
					<div id="notification-popup" class="notice-popup js__toggle" data-space="55">
						<h2 class="popup-title">Your Notifications</h2>
						<!-- /.popup-title -->
						<div class="content">
							<ul class="notice-list">
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/80x80" alt=""></span>
										<span class="name">John Doe</span>
										<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
										<span class="time">10 min</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/80x80" alt=""></span>
										<span class="name">Anna William</span>
										<span class="desc">Like your post: “Facebook Messenger”</span>
										<span class="time">15 min</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar bg-warning"><i class="fa fa-warning"></i></span>
										<span class="name">Update Status</span>
										<span class="desc">Failed to get available update data. To ensure the please contact us.</span>
										<span class="time">30 min</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/128x128" alt=""></span>
										<span class="name">Jennifer</span>
										<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
										<span class="time">45 min</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/80x80" alt=""></span>
										<span class="name">Michael Zenaty</span>
										<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
										<span class="time">50 min</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/80x80" alt=""></span>
										<span class="name">Simon</span>
										<span class="desc">Like your post: “Facebook Messenger”</span>
										<span class="time">1 hour</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar bg-violet"><i class="fa fa-flag"></i></span>
										<span class="name">Account Contact Change</span>
										<span class="desc">A contact detail associated with your account has been changed.</span>
										<span class="time">2 hours</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/80x80" alt=""></span>
										<span class="name">Helen 987</span>
										<span class="desc">Like your post: “Facebook Messenger”</span>
										<span class="time">Yesterday</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/128x128" alt=""></span>
										<span class="name">Denise Jenny</span>
										<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
										<span class="time">Oct, 28</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="avatar"><img src="http://placehold.it/80x80" alt=""></span>
										<span class="name">Thomas William</span>
										<span class="desc">Like your post: “Facebook Messenger”</span>
										<span class="time">Oct, 27</span>
									</a>
								</li>
							</ul>
							<!-- /.notice-list -->
							<a href="#" class="notice-read-more">See more messages <i class="fa fa-angle-down"></i></a>
						</div>
						<!-- /.content -->
					</div>
					<!-- /#notification-popup -->
				</div>
				<!-- /.ico-item -->
				<div class="ico-item">
					<a href="#" class="ico-item fa fa-user js__toggle_open" data-target="#user-status"></a>
					<div id="user-status" class="user-status js__toggle">
						<a href="#" class="avatar"><img src="http://placehold.it/80x80" alt=""><span class="status online"></span></a>
						<h5 class="name"><a href="profile.html">Emily Stanley</a></h5>
						<h5 class="position">Administrator</h5>
						<!-- /.name -->
						<div class="control-items">
							<div class="control-item"><a href="#" title="Settings"><i class="fa fa-gear"></i></a></div>
							<div class="control-item"><a href="#" class="js__logout" title="Log out"><i class="fa fa-power-off"></i></a></div>
						</div>
						<!-- /.control-items -->
					</div>
					<!-- /#user-status -->
				</div>
				<!-- /.ico-item -->
			</div>
			<!-- /.pull-right -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.header-top -->
	<nav class="nav-horizontal">
		<button type="button" class="menu-close hidden-on-desktop js__close_menu"><i class="fa fa-times"></i><span>CLOSE</span></button>
		<div class="container">
			
			<ul class="menu">
					<li <?php 
					if ($menu=='dashboard'){
						echo 'class="current"';
					}
					?>>
						<a href="<?php echo base_url();?>"><i class="ico fa fa-home"></i><span>Dashboard</span></a>
					</li>
					<li <?php 
					if ($menu=='master'){
						echo 'class="current has-sub"';
					} else {
						echo 'class="has-sub"';
					}
					?>>
						<a href="#"><i class="ico fa fa-database"></i><span>Master Data</span></a>
						<ul class="sub-menu single">
							<li><a href="<?php echo base_url();?>status">Status Management</a></li>
							<li><a href="<?php echo base_url();?>unit">Unit Management (new)</a></li>
							<li><a href="<?php echo base_url();?>location">Location Management (new)</a></li>
							<li><a href="<?php echo base_url();?>user">User Management</a></li>
							<li><a href="<?php echo base_url();?>product">Product Management</a></li>
							<li><a href="<?php echo base_url();?>product/other_product">Other Product Management</a></li>
							<li><a href="<?php echo base_url();?>totalisator">Totalisator Management</a></li>
							<li><a href="<?php echo base_url();?>shift">Shift Management</a></li>
						</ul>
					</li>

					<li <?php 
					if ($menu=='operation'){
						echo 'class="current has-sub"';
					} else {
						echo 'class="has-sub"';
					}
					?>>
						<a href="#"><i class="ico fa fa-file-photo-o"></i><span>Operation</span></a>
						<ul class="sub-menu single">
							<li><a href="<?php echo base_url();?>price">Price Management</a></li>
							<li><a href="<?php echo base_url();?>sales">Sales Management</a></li>
							<li><a href="<?php echo base_url();?>sales/other_product">Other Product Sales Management</a></li>
							<li><a href="<?php echo base_url();?>stock">Stock Management</a></li>
							<li><a href="<?php echo base_url();?>expense">Expense Management</a></li>
						</ul>
					</li>
					<li <?php 
					if ($menu=='report'){
						echo 'class="current has-sub"';
					} else {
						echo 'class="has-sub"';
					}
					?>>
						<a href="#"><i class="ico fa fa-file-pdf-o"></i><span>Report</span></a>
						<ul class="sub-menu single">
							<li><a href="<?php echo base_url();?>report/sales_date_range">Sales Report</a></li>
							<li><a href="<?php echo base_url();?>report/stock">Stock Report</a></li>
							<li><a href="<?php echo base_url();?>report/expense">Expense Report</a></li>
							<li><a href="<?php echo base_url();?>report/omzet">Omzet Report</a></li>
							<li><a href="<?php echo base_url();?>report/profit">Profit Report</a></li>
							<li><a href="<?php echo base_url();?>report/driver">Driver Report</a></li>
							<li><a href="<?php echo base_url();?>report/analysis">Advanced Analysis</a></li>
						</ul>
					</li>
					<li <?php 
					if ($menu=='license'){
						echo 'class="current"';
					}
					?>>
						<a href="<?php echo base_url();?>license"><i class="ico fa fa-shield"></i><span>Lisence</span></a>
					</li>
					<li <?php 
					if ($menu=='logout'){
						echo 'class="current"';
					}
					?>>
						<a href="<?php echo base_url();?>userAuth/logout"><i class="ico fa fa-low-vision"></i><span>Logout</span></a>
					</li>

					<!-- -->

			</ul>
			<!-- /.menu -->
		</div>
		<!-- /.container -->
	</nav>
	<!-- /.nav-horizontal -->
</header>
<!-- /.fixed-header -->
