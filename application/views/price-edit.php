
<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Edit a price Data</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('price/edit_process','class="form" id="totalisator_add_form"');?>    
								<div class="form-group">
									<label>Price Date (Bulan/Tanggal/Tahun)</label>
									<div class="input-group col-xs-4">
										<input type="text" name="price_date" id="datepicker" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" value="<?php echo date("m/d/Y", strtotime($price['price_date']));?>">
										<span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
									</div>
                                </div>
								<div class="form-group">
									<label>Price Code</label>
									<input type="text" name="price_id" id="price_id" hidden id="inp-type-1" placeholder="Enter price code" value="<?php echo $price['price_id'];?>">
									<input type="text" name="price_code" id="price_code" class="form-control" id="inp-type-1" placeholder="Enter price code" value="<?php echo $price['price_code'];?>">
								</div>
								<div class="form-group">
									<label>Product</label>
									<select class="form-control" name="product_id" id="product_id" >
										<option value="<?php echo $price['product_id']?>"><?php echo $product_name;?></option>
										<?php foreach ($products as $product) :  ?>
										<option value="<?php echo $product['product_id']?>"><?php echo $product['product_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Price Buy</label>
									<input type="number" name="price_buy" id="price_buy" class="form-control" id="inp-type-1" placeholder="Enter buying price" onchange="calculateDifference()" value="<?php echo $price['price_buy'];?>">
                                </div>
                                <div class="form-group">
									<label>Price Sell</label>
									<input type="number" name="price_sell" id="price_sell" class="form-control" id="inp-type-1" placeholder="Enter selling price" onchange="calculateDifference()" value="<?php echo $price['price_sell'];?>">
                                </div>
                                <script>
                                    function calculateDifference() {
                                    var before = document.getElementById("price_buy").value; 
                                    var after = document.getElementById("price_sell").value; 
                                    var difference = after - before;
                                    document.getElementById("price_difference").value = difference;
                                    }
                                </script>
                                <div class="form-group">
									<label>Price Difference (click me)</label>
									<input type="number" name="price_difference" disabled id="price_difference" class="form-control" id="inp-type-1" placeholder="Enter diffrence of pice" value="<?php echo $price['price_difference'];?>">
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status_id" id="status_id" >
										<option value="<?php echo $price['status_id']?>"><?php echo $status_name;?></option>
										<?php foreach ($statuses as $status) :  ?>
										<option value="<?php echo $status['status_id']?>"><?php echo $status['status_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Price Description</label>
									<textarea class="form-control" name="price_desc" id="price_desc" id="inp-type-5" placeholder="Write totalisator description"><?php echo $price['price_desc'];?></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	