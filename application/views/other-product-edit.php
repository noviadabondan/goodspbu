
<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add an other product</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('product/other_product_edit_process','class="form" id="product_add_form"');?>    
								<div class="form-group">
									<label>Product Code</label>
									<input type="text" name="product_id" id="product_id"  hidden id="inp-type-1" placeholder="Enter product code" value="<?php echo $product['product_id'];?>">
									<input type="text" name="product_code" id="product_code" class="form-control" id="inp-type-1" placeholder="Enter product code" value="<?php echo $product['product_code'];?>">
								</div>
								<div class="form-group">
									<label>Product Name</label>
									<input type="text" name="product_name" id="product_name" class="form-control" id="inp-type-1" placeholder="Enter product name" value="<?php echo $product['product_name'];?>">
								</div>
								<div class="form-group">
									<label>Unit</label>
									<select class="form-control" name="unit_id" id="unit_id" >
										<option value="<?php echo $product['unit_id'];?>"><?php echo $unit_name;?></option>
										<?php foreach ($units as $unit) :  ?>
										<option value="<?php echo $unit['unit_id']?>"><?php echo $unit['unit_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Product Date</label>
									<div class="input-group col-xs-4">
										<input type="text" name="product_date" id="datepicker" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" value="<?php echo $product['product_date'];?>">
										<span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label>Product Image</label>
									<img class="form-control" id="uploadPreview" style="width: 136px; height: 76px;" src="<?php echo base_url();?>assets/img/product/<?php echo $product['product_image']?>"/>
									<input name="product_image" id="uploadImage" type="file" onchange="PreviewImage();" class="form-control"/>
										<script type="text/javascript">

											function PreviewImage() {
												var oFReader = new FileReader();
												oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

												oFReader.onload = function (oFREvent) {
													document.getElementById("uploadPreview").src = oFREvent.target.result;
												};
											};

										</script>
									
								</div>
								<div class="form-group">
									<label>Buy Price</label>
									<input type="text" name="product_price_buy" id="product_price_buy" class="form-control" id="inp-type-1" placeholder="Enter product name" value="<?php echo $product['product_price_buy'];?>">
								</div>
								<div class="form-group">
									<label>Sell Price</label>
									<input type="text" name="product_price_sell" id="product_price_sell" class="form-control" id="inp-type-1" placeholder="Enter product name" value="<?php echo $product['product_price_sell'];?>">
								</div>
								<div class="form-group">
									<label>Product Description</label>
									<textarea class="form-control" name="product_desc" id="product_desc" id="inp-type-5" placeholder="Write product description"><?php echo $product['product_description'];?></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	