<!DOCTYPE html>
<html>
<head>
<!-- JQuery -->
<script src='<?php echo base_url()?>assets/backoffice/assets/jquery-3.1.1.min.js' type='text/javascript'></script>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 33.33%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>
<body>

<h2>Good SPBU  - Sales Report</h2>
<h3>Date : <?php echo $report_start_date.' Until '.$report_end_date;?></h3>


  <?php 
  
        $begin = new DateTime($report_start_date);
        $end = new DateTime($report_end_date);
        $end = $end->modify( '+1 day' ); 

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval ,$end);

        foreach($daterange as $date) : ?>
    <br>
    <h3>List Harga BBM Tanggal <?php echo $date->format("Y-m-d"); ?></h3>

    <table>
        <tr>
            <th>Produk</th>
            <th>Harga Beli</th>
            <th>Harga Jual</th>
            <th>Margin</th>
        </tr>
        <?php foreach($products as $product) : ?>
        <tr>
            <td><?php echo $product['product_name'];?></td>
            <td class=""><input disabled id="price-buy-<?php echo $date->format("Y-m-d").'-'.$product['product_id'];?>"></input></td>
            <td class=""><input disabled id="price-sell-<?php echo $date->format("Y-m-d").'-'.$product['product_id'];?>"></input></td>
            <td class=""><input disabled id="price-difference-<?php echo $date->format("Y-m-d").'-'.$product['product_id'];?>"></input></td>
        </tr>
            <!-- mencari data totalisator dengan ID -->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>price/get_price_by_date_and_product_id/<?php echo $date->format("Y-m-d").'/'.$product['product_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
                          var obj = JSON.parse(response);
                          document.getElementById('price-sell-<?php echo $date->format("Y-m-d")."-".$product['product_id'];?>').value = obj.price_sell;
                          document.getElementById('price-buy-<?php echo $date->format("Y-m-d")."-".$product['product_id'];?>').value = obj.price_buy;
                          document.getElementById('price-difference-<?php echo $date->format("Y-m-d")."-".$product['product_id'];?>').value = obj.price_difference;
												}
											});
									});
								</script>
        <?php endforeach; ?>
    </table>  
      <h3><?php echo $date->format("Y-m-d");?></h3>
      <div class="row">  
      <?php foreach($shifts as $shift) : ?>
          <?php echo $shift['shift_name']; ?>
          
          <div class="column">
          <table>
						<tbody> 
							<tr> 
								<?php foreach ($products as $product) :  ?>
									<div class="form-group">
										<h4><?php echo $product['product_name'];?></h4>
                    
										<table class="">
											<tbody> 
											
												<tr> 
													<?php foreach ($totalisators as $totalisator) :  ?>
													<?php if($totalisator['product_id']==$product['product_id']){
														echo '<th scope="row">'.$totalisator["totalisator_name"].''.'</th> ';
														}?>
														
													<?php endforeach; //totalisator?>
												</tr> 
                        <tr> 
													<?php foreach ($totalisators as $totalisator) :  ?>
													<?php if($totalisator['product_id']==$product['product_id']){
														echo '<th scope="row"> Before : '.$totalisator["totalisator_name"].''.'</th> ';
														}?>
														
													<?php endforeach; //totalisator?>
												</tr> 
                        <tr> 
													<?php foreach ($totalisators as $totalisator) :  ?>
													<?php if($totalisator['product_id']==$product['product_id']){
														echo '<th scope="row">After : '.$totalisator["totalisator_name"].''.'</th> ';
														}?>
														
													<?php endforeach; //totalisator?>
												</tr> 

                        <tr> 
													<?php foreach ($totalisators as $totalisator) :  ?>
													<?php if($totalisator['product_id']==$product['product_id']){
														echo '<th scope="row">Difference : '.$totalisator["totalisator_name"].''.'</th> ';
														}?>
														
													<?php endforeach; //totoaloisator?>
												</tr>
											
											</tbody> 
										</table>
									</div>
								<?php endforeach; //produk?>
							</tr> 
						</tbody> 
					</table>
          </div>
       
          
          
        <?php endforeach; //shift?>
        </div>

        
      
    <?php endforeach; //date?> 



  
  




</body>
</html>
