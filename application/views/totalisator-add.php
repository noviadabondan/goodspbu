
<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add a totalisator</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('totalisator/add_process','class="form" id="totalisator_add_form"');?>    
								<div class="form-group">
									<label>Totalisator Code</label>
									<input type="text" name="totalisator_code" id="totalisator_code" class="form-control" id="inp-type-1" placeholder="Enter totalisator code">
								</div>
								<div class="form-group">
									<label>Totalisator Name</label>
									<input type="text" name="totalisator_name" id="totalisator_name" class="form-control" id="inp-type-1" placeholder="Enter totalisator name">
								</div>
								<div class="form-group">
									<label>Product</label>
									<select class="form-control" name="product_id" id="product_id" >
										<?php foreach ($products as $product) :  ?>
										<option value="<?php echo $product['product_id']?>"><?php echo $product['product_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Current Position</label>
									<input type="text" name="totalisator_current_position" id="totalisator_current_position" class="form-control" id="inp-type-1" placeholder="Enter totalisator position">
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status_id" id="status_id" >
										<?php foreach ($statuses as $status) :  ?>
										<option value="<?php echo $status['status_id']?>"><?php echo $status['status_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Totalisator Description</label>
									<textarea class="form-control" name="totalisator_desc" id="totalisator_desc" id="inp-type-5" placeholder="Write totalisator description"></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	