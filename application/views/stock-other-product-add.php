

<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add a Shift</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('stock/other_product_add_process','class="form" id="user_add_form"');?>    
								<div class="form-group">
									<label>Stock</label>
									<input type="text" hidden name="unit_id" id="unit_id" id="inp-type-1" placeholder="Enter Stock Ammount Received" value="<?php echo $unit_id; ?>">
									<input type="text" hidden name="product_id" id="product_id" id="inp-type-1" placeholder="Enter Stock Ammount Received" value="<?php echo $product_id; ?>">
									<input type="text" name="stock_amount" id="stock_amount" class="form-control" id="inp-type-1" placeholder="Enter Stock Ammount Received">
								</div>
								<div class="form-group">
									<label>Location</label>
									<select class="form-control" name="location_id" id="location_id" >
										<?php foreach ($locations as $location) :  ?>
										<option value="<?php echo $location['location_id']?>"><?php echo $location['location_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea class="form-control" name="stock_desc" id="stock_desc" id="inp-type-5" placeholder="Write your meassage"></textarea>
								</div>
								
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	
