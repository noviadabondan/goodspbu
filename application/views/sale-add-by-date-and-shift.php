<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content card white">
						<h4 class="box-title">Add a Sale in Simple Way</h4>
						<!-- /.box-title -->
						<div class="card-content">
                            <?php echo form_open_multipart('sales/add_process_by_date_and_shift','class="form" id="product_add_form"');?>  
								<div class="form-group">
									<label>User</label>
									<input type="text" name="user_email"  hidden id="user_email" id="inp-type-1" placeholder="Enter product code" value="<?php echo $user_email?>">
									<input type="text" name="user_email_disabled"  disabled id="user_email_disabled" class="form-control" id="inp-type-1" placeholder="Enter product code" value="<?php echo $user_email?>">
								</div>
								<div class="form-group">
									<label>Sales Date (Bulan/Tanggal/Tahun)</label>
									<div class="input-group col-xs-4">
										<input type="text" name="sale_date" id="datepicker" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" value="<?php echo date("m/d/yy");?>">
										<span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
									</div>
                                </div>
                                <div class="form-group">
									<label>Shift</label>
									<select class="form-control" name="shift_id" id="shift_id" onchange="getView()" >
											<option value="0">Pilih Shift</option>
										<?php foreach ($shifts as $shift) :  ?>
										    <option value="<?php echo $shift['shift_id']?>"><?php echo $shift['shift_name']?></option>
										<?php endforeach; ?>
									</select>
                                </div>
									<script>
                                        function getView() {
                                        var date = document.getElementById("datepicker").value;
										var shift_id = document.getElementById("shift_id").value;
										var newdateformat1 = date.replace("/", "-");
										var newdateformat2 = newdateformat1.replace("/", "-");
                                        //var text_selection = document.getElementById("totalisator_id").options[document.getElementById("totalisator_id").selectedIndex ].text
                                        //substring from the last
                                        //var product_id = text_selection.substr(text_selection.length - 1); // => "1"
                                        //document.getElementById("product_id").value = product_id;
                                        //AJAX request
											$.ajax({
												url: '<?php echo base_url();?>product/get_product_view/'+newdateformat2+'/'+shift_id,
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													// $('.modal-body<?php //echo $totalisator['totalisator_id'];?>').html(response); 
													// // Display Modal
                                                    // $('#boostrapModal-<?php //echo $totalisator['totalisator_id'];?>').modal('show'); 
                                                    $('.product-view').html(response);
												}
											});
											$.ajax({
												url: '<?php echo base_url();?>price/get_price/'+product_id,
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													// $('.modal-body<?php //echo $totalisator['totalisator_id'];?>').html(response); 
													// // Display Modal
                                                    // $('#boostrapModal-<?php //echo $totalisator['totalisator_id'];?>').modal('show'); 
                                                    $('.product-price').html("Price Used : IDR "+response);
												}
											});
                                        }
                                    </script>
								<div class="product-view">
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status_id" id="status_id" >
										<?php foreach ($statuses as $status) :  ?>
										<option value="<?php echo $status['status_id']?>"><?php echo $status['status_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Sale Note</label>
									<textarea class="form-control" name="sale_desc" id="sale_desc" id="inp-type-5" placeholder="Write your message"></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	

