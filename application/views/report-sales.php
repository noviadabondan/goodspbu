<!DOCTYPE html>
<html>
<head>
<script src='<?php echo base_url()?>assets/backoffice/assets/jquery-3.1.1.min.js' type='text/javascript'></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 33.33%;
  padding: 10px; /* Should be removed. Only for demonstration */
}

.columndevider { /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>
<body>
<h2>Good SPBU  - Sales Report</h2>
<h3>Date : <?php echo $report_start_date.' Until '.$report_end_date;?></h3>


  <?php 
  
        $begin = new DateTime($report_start_date);
        $end = new DateTime($report_end_date);
        $end = $end->modify( '+1 day' ); 

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval ,$end);

        foreach($daterange as $date) : ?>
    <br>
    <h3>List Harga BBM Tanggal <?php echo $date->format("Y-m-d"); ?></h3>

    <table>
        <tr>
            <th>Produk</th>
            <th>Harga Beli</th>
            <th>Harga Jual</th>
            <th>Margin</th>
        </tr>
        <?php foreach($products as $product) : ?>
        <tr>
            <td><?php echo $product['product_name'];?></td>
            <td class=""><input disabled id="price-buy-<?php echo $date->format("Y-m-d").'-'.$product['product_id'];?>"></input></td>
            <td class=""><input disabled id="price-sell-<?php echo $date->format("Y-m-d").'-'.$product['product_id'];?>"></input></td>
            <td class=""><input disabled id="price-difference-<?php echo $date->format("Y-m-d").'-'.$product['product_id'];?>"></input></td>
        </tr>
            <!-- mencari data totalisator dengan ID -->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>price/get_price_by_date_and_product_id/<?php echo $date->format("Y-m-d").'/'.$product['product_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
                          var obj = JSON.parse(response);
                          document.getElementById('price-sell-<?php echo $date->format("Y-m-d")."-".$product['product_id'];?>').value = obj.price_sell;
                          document.getElementById('price-buy-<?php echo $date->format("Y-m-d")."-".$product['product_id'];?>').value = obj.price_buy;
                          document.getElementById('price-difference-<?php echo $date->format("Y-m-d")."-".$product['product_id'];?>').value = obj.price_difference;
												}
											});
									});
								</script>
        <?php endforeach; ?>
    </table>  

    <h2><?php echo $date->format("Y-m-d"); ?></h2>
      <div class="row">
          <?php foreach($shifts as $shift) : ?>
            <div class="column" style="background-color:#fff;">
              <h2><?php echo $shift['shift_name'];?></h2>
              <script>
                  var res = 0;
                </script>
              <?php foreach ($products as $product) :  ?>
                <h4><?php echo $product['product_name'];?></h4>
                <?php foreach ($totalisators as $totalisator) :  ?>
                  <?php if($totalisator['product_id']==$product['product_id']){
                            echo $totalisator["totalisator_name"].'<div class="row"><div class="column">Before<br><input id="totalisator-before-'.$date->format("Y-m-d").'/'.$shift['shift_id'].'/'.$totalisator['totalisator_id'].'"></input></div><div class="column">After<br><input id="totalisator-after-'.$date->format("Y-m-d").'/'.$shift['shift_id'].'/'.$totalisator['totalisator_id'].'"></input></div><div class="column">Difference<br><input id="totalisator-difference-'.$date->format("Y-m-d").'/'.$shift['shift_id'].'/'.$totalisator['totalisator_id'].'"></input></div></div>';
														}?>
                    <!-- mencari data totalisator dengan ID -->
                    <script type='text/javascript'>
                      $(document).ready(function(){
                          // AJAX request
                         var a = 1;
                          $.ajax({
                            url: '<?php echo base_url();?>sales/get_sale_by_date_shift_totalisator/<?php echo $date->format("Y-m-d").'/'.$shift['shift_id'].'/'.$totalisator['totalisator_id'];?>',
                            type: 'get',
                            success: function(response){ 
                              // Add response in Modal body
                              var obj = JSON.parse(response);
                              document.getElementById('totalisator-before-<?php echo $date->format("Y-m-d").'/'.$shift['shift_id'].'/'.$totalisator['totalisator_id'];?>').value = obj.totalisator_position_before;
                              document.getElementById('totalisator-after-<?php echo $date->format("Y-m-d").'/'.$shift['shift_id'].'/'.$totalisator['totalisator_id'];?>').value = obj.totalisator_position_after;
                              document.getElementById('totalisator-difference-<?php echo $date->format("Y-m-d").'/'.$shift['shift_id'].'/'.$totalisator['totalisator_id'];?>').value = obj.totalisator_difference;
                              a = a + 1;
                            }
                          });
                      });
                    </script>
                    <script>
                      
                    </script>
                    
                <?php endforeach;?>
                <br>
              <?php endforeach;?>
            </div>
          <?php endforeach;?>
      </div>
    <?php endforeach; ?>



</body>
</html>