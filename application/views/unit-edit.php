<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add a status</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('unit/edit_process','class="form" id="status_add_form"');?>    
								<div class="form-group">
									<label>Unit Name</label>
									<input type="text" name="unit_id" id="unit_id" value="<?php echo $unit['unit_id'];?>" hidden>
									<input type="text" name="unit_name" id="unit_name" class="form-control" id="inp-type-1" placeholder="Enter unit name" value="<?php echo $unit['unit_name'];?>">
								</div>
								<div class="form-group">
									<label>Unit Value</label>
									<input type="text" name="unit_value" id="unit_value" class="form-control" id="inp-type-1" placeholder="Enter unit value" value="<?php echo $unit['unit_value'];?>">
								</div>
								<div class="form-group">
									<label>Unit Description</label>
									<textarea class="form-control" name="unit_desc" id="unit_desc" id="inp-type-5" placeholder="Write unit description"><?php echo $unit['unit_desc'];?></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	