<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add an Other Product Sales</h4>
                        
						<!-- /.box-title -->
						<div class="card-content">
                        <td><button type="button" class="add-row btn btn-success btn-sm waves-effect waves-light">Add Row Product<i class="ico ico-right fa fa-plus"></i></button></td> 
							<?php echo form_open_multipart('test/view_data','class="form" id="other_product_sale_add"');?>  
                            <input id="lineProduct" name="lineProduct" value="" hidden></input>  
                            <table class="table"> 
                                <thead> 
                                    <tr> 
                                        <th>Product</th> 
                                        <th>Price</th> 
                                        <th>Qty</th> 
                                        <th>Sub Total</th>
                                        <th>Note</th>
                                    </tr> 
                                </thead> 
                                <tbody>
                                    <script>
                                        function changePrice(x) {
                                        var product_id = document.getElementById("product_id"+x).value;
                                        // mencari data shift dengan ID 
                                        $(document).ready(function(){
                                                // AJAX request
                                                $.ajax({
                                                    url: '<?php echo base_url();?>product/get_other_product_price/'+product_id,
                                                    type: 'get',
                                                    success: function(response){ 
                                                        // Add response in Modal body
                                                        document.getElementById("price"+x).value = response;
                                                    }
                                                });
                                        });
                                        console.log(product_id);
                                        }
                                    </script>

                                    <script>
                                    function calSubTotal() {
                                        x = 33;
                                        console.log(x);
                                        }
                                    </script>
                                    
                                </tbody> 
                            </table> 
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<script> 
    let lineNo = 1; 
    $(document).ready(function () { 
        $('.add-row').click(function () { 
            markup = '<tr><td><select onchange="changePrice('+lineNo+')" id="product_id'+lineNo+'" name="product_id'+lineNo+'" class="select2_1"><option value="">Select Product to Sell</option><?php foreach ($products as $product) :  ?><option value="<?php echo $product['product_id'];?>"><?php echo $product['product_name'];?></option><?php endforeach;  ?></select></td><td><input onkeyup="this.calSubTotal();" value="1" id="price'+lineNo+'" name="price'+lineNo+'"></input></td><td><input name="qty" type="number" name="qty" value="6"></input></td><td>data 2</td><td><textarea>data harus jelas</textarea></td><tr>';
            tableBody = $('table tbody'); 
            tableBody.append(markup); 
            lineNo++; 
            $('.select2_1').select2();
            document.getElementById("lineProduct").value = lineNo-1;
        }); 
    });  
</script>
<!-- /.row -->	


