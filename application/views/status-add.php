<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add a status</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('status/add_process','class="form" id="status_add_form"');?>    
								<div class="form-group">
									<label>Status Name</label>
									<input type="text" name="status_name" id="status_name" class="form-control" id="inp-type-1" placeholder="Enter status name">
								</div>

								<div class="form-group">
									<label>Status For (Feature)</label>
									<select class="form-control" name="status_for" id="status_for" >
										<option value="">Nothing selected</option>
										<option value="user">user</option>
										<option value="product">product</option>
										<option value="totalisator">totalisator</option>
										<option value="shift">shift</option>
										<option value="sale">sale</option>
										<option value="price">price</option>
									</select>
								</div>
								<div class="form-group">
									<label>Status Value (optional)</label>
									<input type="text" name="status_value" id="status_value" class="form-control" id="inp-type-1" placeholder="Enter status value">
								</div>
								<div class="form-group">
									<label>Status Description</label>
									<textarea class="form-control" name="status_desc" id="status_desc" id="inp-type-5" placeholder="Write status description"></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	