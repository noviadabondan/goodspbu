<?php 
	$id = "";
?>

<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">


            <?php foreach ($products as $product) :  ?>
            <?php //echo $product['product_name'];?>
            <?php endforeach;?>

            
            <div class="box-content">
			<a href="<?php echo base_url();?>product/add" type="button" class="btn btn-success waves-effect waves-light">+ Add Data</a>
            </div>
            </div>
            

            <div class="col-xs-12">
				<div class="box-content">
                    <h4 class="box-title">Product Data</h4>
                    
					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						<ul class="sub-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else there</a></li>
							<li class="split"></li>
							<li><a href="#">Separated link</a></li>
						</ul>
						<!-- /.sub-menu -->
					</div>
					<!-- /.dropdown js__dropdown -->
					<table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
							<tr>
								<th>Code</th>
								<th>Product</th>
								<th>Desc</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
                                <th>Code</th>
								<th>Product</th>
								<th>Desc</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
                            <?php foreach ($products as $product) :  ?>
							<tr>
                                <td><?php echo $product['product_code'];?></td>
                                <td><?php echo $product['product_name'];?></td>
                                <td><?php echo $product['product_description'];?></td>
                                <td>
									<button data-id='<?php echo $product['product_id'];?>' id="productInfo<?php echo $product['product_id'];?>" class='productInfo<?php echo $product['product_id'];?> btn btn-success btn-circle btn-xs waves-effect waves-light'><i class="ico fa fa-eye"></i></button>
									<a type="button" href="<?php echo base_url();?>product/edit/<?php echo $product['product_id']; ?>" class="btn btn-primary btn-circle btn-xs waves-effect waves-light"><i class="ico fa fa-edit"></i></a>
									<button type="button" class="btn btn-danger btn-circle btn-xs waves-effect waves-light" data-toggle="modal" onclick="changeValue(<?php echo $product['product_id']; ?>)" data-target="#boostrapModalDelete-<?php echo $product['product_id'];?>"><i class="ico fa fa-trash"></i></button>
                                </td>
                            </tr>
							<script type='text/javascript'>
							
										$(document).on('click', '.productInfo<?php echo $product['product_id'];?>', function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>product/get_product_detail/<?php echo $product['product_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.modal-body<?php echo $product['product_id'];?>').html(response); 

													// Display Modal
													$('#boostrapModal-<?php echo $product['product_id'];?>').modal('show'); 
												}
											});
										});
									
								</script>	
                            <?php endforeach;?>
						</tbody>
					</table>
				</div>
					<script>
						function changeValue(str) {
							var x = 1;
						}
						function deleteproduct(x) {
						var xhttp;
						if (x.length == 0) { 
							document.getElementById("txtHint").innerHTML = "";
							return;
						}
						xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
							document.getElementById("txtHint").innerHTML = this.responseText;
							location.reload();
							}
						};
						xhttp.open("GET", "product/delete/"+x, true);
						xhttp.send();   
						}
					</script>
				<!-- /.box-content -->
			</div>
        </div>
	</div>
</div>

<!-- Modal Detail -->
<?php foreach ($products as $product) :  ?>
<div class="modal fade" id="boostrapModal-<?php echo $product['product_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">User Detail</h4>
			</div>
			<div class="modal-body<?php echo $product['product_id'];?>">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm waves-effect waves-light" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<?php foreach ($products as $product) :  ?>
<div class="modal fade" id="boostrapModalDelete-<?php echo $product['product_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">Delete Data</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin untuk menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm waves-effect waves-light" data-dismiss="modal">No</button>
				<a type="button" class="btn btn-danger btn-sm waves-effect waves-light" href="<?php echo base_url();?>product/delete/<?php echo $product['product_id'];?>" >Yes</a>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>