<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Add a Location</h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('location/edit_process','class="form" id="status_add_form"');?>    
								<div class="form-group">
									<label>Location Name</label>
									<input type="text" name="location_name" id="location_name" class="form-control" id="inp-type-1" placeholder="Enter location name" value="<?php echo $location['location_name'];?>">
									<input type="text" name="location_id" id="location_id" id="inp-type-1" hidden placeholder="Enter location name" value="<?php echo $location['location_id'];?>">
								</div>
								<div class="form-group">
									<label>Location Value</label>
									<input type="text" name="location_value" id="location_value" class="form-control" id="inp-type-1" placeholder="Enter location value" value="<?php echo $location['location_value'];?>">
								</div>
								<div class="form-group">
									<label>Location Logitude</label>
									<input type="text" name="location_long" id="location_long" class="form-control" id="inp-type-1" placeholder="Enter location longitude" value="<?php echo $location['location_long'];?>">
								</div>
								<div class="form-group">
									<label>Location Latitude</label>
									<input type="text" name="location_lat" id="location_lat" class="form-control" id="inp-type-1" placeholder="Enter location latitude" value="<?php echo $location['location_lat'];?>">
								</div>
								<div class="form-group">
									<label>Location Description</label>
									<textarea class="form-control" name="location_desc" id="location_desc" id="inp-type-5" placeholder="Write location description"><?php echo $location['location_desc'];?></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Submit</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->	