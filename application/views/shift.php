<?php 
	$id = "";
?>

<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
            <div class="box-content">
			<a href="<?php echo base_url();?>shift/add" type="button" class="btn btn-success waves-effect waves-light">+ Add Data</a>
            </div>
            </div>
            

            <div class="col-xs-12">
				<div class="box-content">
                    <h4 class="box-title">Shift Data</h4>
					<!-- /.dropdown js__dropdown -->
					<table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
							<tr>
								<th>Code</th>
								<th>Shift</th>
								<th>Start Time</th>
								<th>Stop Time</th>
								<th>Desc</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Code</th>
								<th>Shift</th>
								<th>Start Time</th>
								<th>Stop Time</th>
								<th>Desc</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
                            <?php foreach ($shifts as $shift) :  ?>
							<tr>
                                <td><?php echo $shift['shift_code'];?></td>
                                <td><?php echo $shift['shift_name'];?></td>
								<td><?php echo $shift['shift_start'];?></td>
								<td><?php echo $shift['shift_stop'];?></td>
								<td><?php echo $shift['shift_desc'];?></td>
                                <td>
									<a type="button" href="<?php echo base_url();?>shift/edit/<?php echo $shift['shift_id']; ?>" class="btn btn-primary btn-circle btn-xs waves-effect waves-light"><i class="ico fa fa-edit"></i></a>
									
                                </td>
                            </tr>
							<script type='text/javascript'>
							
										$(document).on('click', '.productInfo<?php echo $shift['shift_id'];?>', function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>product/get_shift_detail/<?php echo $shift['shift_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.modal-body<?php echo $shift['shift_id'];?>').html(response); 

													// Display Modal
													$('#boostrapModal-<?php echo $shift['shift_id'];?>').modal('show'); 
												}
											});
										});
									
								</script>	

								<!-- mencari data produk dengan ID -->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>product/get_product_name/<?php echo $shift['product_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.product-name<?php echo $shift['shift_id'];?>').html(response);  
												}
											});
									});
								</script>

								<!-- mencari data nama status dengan ID  dan menggantinya ke ID target-->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>status/get_status_name/<?php echo $shift['status_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.status-name<?php echo $shift['shift_id'];?>').html(response);  
												}
											});
									});
								</script>
                            <?php endforeach;?>
						</tbody>
					</table>
				</div>
					<script>
						function changeValue(str) {
							var x = 1;
						}
						function deleteproduct(x) {
						var xhttp;
						if (x.length == 0) { 
							document.getElementById("txtHint").innerHTML = "";
							return;
						}
						xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
							document.getElementById("txtHint").innerHTML = this.responseText;
							location.reload();
							}
						};
						xhttp.open("GET", "product/delete/"+x, true);
						xhttp.send();   
						}
					</script>
				<!-- /.box-content -->
			</div>
        </div>
	</div>
</div>

<!-- Modal Detail -->
<?php foreach ($shifts as $shift) :  ?>
<div class="modal fade" id="boostrapModal-<?php echo $shift['shift_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">User Detail</h4>
			</div>
			<div class="modal-body<?php echo $shift['shift_id'];?>">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm waves-effect waves-light" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<?php foreach ($shifts as $shift) :  ?>
<div class="modal fade" id="boostrapModalDelete-<?php echo $shift['shift_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">Delete Data</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin untuk menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm waves-effect waves-light" data-dismiss="modal">No</button>
				<a type="button" class="btn btn-danger btn-sm waves-effect waves-light" href="<?php echo base_url();?>shift/delete/<?php echo $shift['shift_id'];?>" >Yes</a>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>