
<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
			<div class="box-content">
					<h4 class="box-title">Sales Report</h4>
					<?php echo form_open_multipart('report/sales','class="form" id="product_add_form"');?>  
					<div class="form-group">
									<label class="control-label col-sm-4">Select the date range</label>
									<div class="col-sm-8">
										<div class="input-daterange input-group" id="date-range">
											<input type="text" class="form-control" name="report_start_date" id="report_start_date">
											<span class="input-group-addon bg-primary text-white">to</span>
											<input type="text" class="form-control" name="report_end_date" id="report_end_date">
										</div>
										<br>
										<button type="submit" class="btn btn-primary waves-effect waves-light">Show Report</button>
									</div>
								</div>
					</form>			
					<!-- /#smil-animation-chartist-chart.chartist-chart -->
				</div>
				<!-- /.box-content -->
			</div>
		</div>
		<!-- /.row -->	