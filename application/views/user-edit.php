<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
				
				<div class="box-content card white">
						<h4 class="box-title">Edit a User : <?php echo $user['user_name'];?> </h4>
						<!-- /.box-title -->
						<div class="card-content">
							<?php echo form_open_multipart('user/edit_process','class="form" id="user_add_form"');?>    
								<div class="form-group">
									<label>User Name</label>
									<input type="text" name="user_name" id="user_name" class="form-control" id="inp-type-1" placeholder="Enter your Name" value="<?php echo $user['user_name'];?>">
									<input type="text" name="user_id"  hidden value="<?php echo $user['user_id'];?>">
								</div>
								<div class="form-group">
									<label>Email address</label>
									<input type="email" name="user_email" id="user_email" class="form-control" id="exampleInputEmail1" placeholder="Enter your email" value="<?php echo $user['user_email'];?>">
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="user_status" id="user_status" >
										<option value="<?php echo $user['user_status'];?>"><?php echo $user['user_status'];?></option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</div>
								<div class="form-group">
									<label>Date of Birth</label>
									<div class="input-group col-xs-4">
										<input type="text" name="user_dob" id="datepicker" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" value="<?php echo date("d-m-Y", strtotime($user['user_dob']));?>">
										<span class="input-group-addon bg-primary text-white"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea class="form-control" name="user_desc" id="user_desc" id="inp-type-5" placeholder="Write your meassage"><?php echo $user['user_name'];?></textarea>
								</div>
								<div class="form-group">
									<label>File input</label>
									<img class="form-control" id="uploadPreview" style="width: 136px; height: 76px;" src="<?php echo base_url();?>assets/img/user/<?php echo $user['user_profile_pict']?>"/>
									<input name="user_profile_pict" id="uploadImage" type="file" onchange="PreviewImage();" class="form-control"/>
										<script type="text/javascript">

											function PreviewImage() {
												var oFReader = new FileReader();
												oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

												oFReader.onload = function (oFREvent) {
													document.getElementById("uploadPreview").src = oFREvent.target.result;
												};
											};

										</script>
									
								</div>
								<div class="form-group">
									<label>File input</label>
									<input type="file" id="exampleInputFile" name="user_name" id="user_name" >
									<p class="help-block">Example block-level help text here.</p>
								</div>
								<div class="checkbox margin-bottom-20">
									<input type="checkbox" id="chk-1"><label for="chk-1">Check me out</label> 
								</div>
								<button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Update Data</button>
							</form>
						</div>
						<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>
		<!-- /.row -->	