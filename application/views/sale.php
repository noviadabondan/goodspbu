<?php 
	$id = "";
?>

<div id="wrapper">
	<div class="main-content container">
		<div class="row small-spacing">
			<div class="col-xs-12">
            <div class="box-content">
			<a href="<?php echo base_url();?>sales/add" type="button" class="btn btn-success waves-effect waves-light">+ Add Data</a>
			<a href="<?php echo base_url();?>sales/add_simple_way" type="button" class="btn btn-success waves-effect waves-light">+ Add Data By Totalisator</a>
			<a href="<?php echo base_url();?>sales/add_sale_by_date_and_shift" type="button" class="btn btn-success waves-effect waves-light">+ Add Data By Date and Shift</a>
            </div>
            </div>
            

            <div class="col-xs-12">
				<div class="box-content">
                    <h4 class="box-title">Sale Data</h4>
					<!-- /.dropdown js__dropdown -->
					<table id="" class="table table-striped table-bordered display" style="width:100%">
						<thead>
							<tr>
								<th>Date</th>
								<th>Shift</th>
								<th>Product</th>
								<th>Totalisator</th>
								<th>Price Used</th>
								<th>Status</th>
								<th>Note</th>
								<th>User</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Date</th>
								<th>Shift</th>
								<th>Product</th>
								<th>Totalisator</th>
								<th>Price Used</th>
								<th>Status</th>
								<th>Note</th>
								<th>User</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
                            <?php foreach ($sales as $sale) :  ?>
							<tr>
                                <td><?php echo $sale['sale_date'];?></td>
								<td class="shift-name<?php echo $sale['sale_id'];?>" >
                                <td class="product-name<?php echo $sale['sale_id'];?>" >
								<td> <p class="totalisator-name<?php echo $sale['sale_id'];?>"></p> <?php echo 'Before : '.$sale['totalisator_position_before'].'<br>After : '.$sale['totalisator_position_after'].'<br>Difference : '.$sale['totalisator_difference'];?></td>
								<td><?php echo $sale['sale_sell_price'];?></td>
								<td class="status-name<?php echo $sale['sale_id'];?>" >
								<td><?php echo $sale['sale_desc'];?></td>
								<td><?php echo $sale['user_email'];?></td>
                                <td>
									<button data-id='<?php echo $sale['sale_id'];?>' id="productInfo<?php echo $sale['sale_id'];?>" class='productInfo<?php echo $sale['sale_id'];?> btn btn-success btn-circle btn-xs waves-effect waves-light'><i class="ico fa fa-eye"></i></button>
									<a type="button" href="<?php echo base_url();?>sales/edit/<?php echo $sale['sale_id']; ?>" class="btn btn-primary btn-circle btn-xs waves-effect waves-light"><i class="ico fa fa-edit"></i></a>
									<button type="button" class="btn btn-danger btn-circle btn-xs waves-effect waves-light" data-toggle="modal" onclick="changeValue(<?php echo $sale['sale_id']; ?>)" data-target="#boostrapModalDelete-<?php echo $sale['sale_id'];?>"><i class="ico fa fa-trash"></i></button>
                                </td>
                            </tr>
							<script type='text/javascript'>
							
										$(document).on('click', '.productInfo<?php echo $sale['sale_id'];?>', function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>product/get_sale_detail/<?php echo $sale['sale_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.modal-body<?php echo $sale['sale_id'];?>').html(response); 

													// Display Modal
													$('#boostrapModal-<?php echo $sale['sale_id'];?>').modal('show'); 
												}
											});
										});
									
								</script>	

								<!-- mencari data totalisator dengan ID -->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>totalisator/get_totalisator_name/<?php echo $sale['totalisator_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.totalisator-name<?php echo $sale['sale_id'];?>').html(response);  
												}
											});
									});
								</script>

								<!-- mencari data shift dengan ID -->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>shift/get_shift_name/<?php echo $sale['shift_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.shift-name<?php echo $sale['sale_id'];?>').html(response);  
												}
											});
									});
								</script>

								<!-- mencari data produk dengan ID -->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>product/get_product_name/<?php echo $sale['product_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.product-name<?php echo $sale['sale_id'];?>').html(response);  
												}
											});
									});
								</script>

								<!-- mencari data nama status dengan ID  dan menggantinya ke ID target-->
								<script type='text/javascript'>
									$(document).ready(function(){
											// AJAX request
											$.ajax({
												url: '<?php echo base_url();?>status/get_status_name/<?php echo $sale['status_id'];?>',
												type: 'get',
												success: function(response){ 
													// Add response in Modal body
													$('.status-name<?php echo $sale['sale_id'];?>').html(response);  
												}
											});
									});
								</script>
                            <?php endforeach;?>
						</tbody>
					</table>
				</div>
					<script>
						function changeValue(str) {
							var x = 1;
						}
						function deleteproduct(x) {
						var xhttp;
						if (x.length == 0) { 
							document.getElementById("txtHint").innerHTML = "";
							return;
						}
						xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
							document.getElementById("txtHint").innerHTML = this.responseText;
							location.reload();
							}
						};
						xhttp.open("GET", "product/delete/"+x, true);
						xhttp.send();   
						}
					</script>
				<!-- /.box-content -->
			</div>
        </div>
	</div>
</div>

<!-- Modal Detail -->
<?php foreach ($sales as $sale) :  ?>
<div class="modal fade" id="boostrapModal-<?php echo $sale['sale_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">User Detail</h4>
			</div>
			<div class="modal-body<?php echo $sale['sale_id'];?>">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm waves-effect waves-light" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>

<!-- Modal Delete -->
<?php foreach ($sales as $sale) :  ?>
<div class="modal fade" id="boostrapModalDelete-<?php echo $sale['sale_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-2">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-2">Delete Data</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin untuk menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm waves-effect waves-light" data-dismiss="modal">No</button>
				<a type="button" class="btn btn-danger btn-sm waves-effect waves-light" href="<?php echo base_url();?>sales/delete/<?php echo $sale['sale_id'];?>" >Yes</a>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>