<?php
class StatusModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_statuses($status_id = FALSE){
        if($status_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_status");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_status WHERE status_id =".$status_id);
        return $query->row_array();
  }

  public function get_statuses_for_totalisator(){
    $query = $this->db->query("SELECT * FROM tb_status WHERE status_for='totalisator'");
    return $query->result_array();
  }

  public function get_statuses_for_sale(){
    $query = $this->db->query("SELECT * FROM tb_status WHERE status_for='sale'");
    return $query->result_array();
  }

  public function get_statuses_for_price(){
    $query = $this->db->query("SELECT * FROM tb_status WHERE status_for='price'");
    return $query->result_array();
  }

  public function add_status(){
		$data = array(
      'status_name' => $this->input->post('status_name'),
      'status_value' => $this->input->post('status_value'),
			'status_for' => $this->input->post('status_for'),
      'status_desc' => $this->input->post('status_desc')
		);
		return $this->db->insert('tb_status',$data);
  }

  public function update_status($additional_data = FALSE)
  {
    $data = array(
      'status_name' => $this->input->post('status_name'),
      'status_value' => $this->input->post('status_value'),
			'status_for' => $this->input->post('status_for'),
      'status_desc' => $this->input->post('status_desc')
		);
    $this->db->where('status_id', $this->input->post('status_id'));
    return $this->db->update('tb_status', $data);
  }

  public function status_delete($id){
		$this->db->where('status_id', $id);
		return $this->db->delete('tb_status');
	}
}
