<?php
class StockModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_data($stock_id = FALSE){
        if($stock_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_stock");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_stock WHERE stock_id =".$stock_id);
        return $query->row_array();
  }

  public function get_other_product_stock($product_id = FALSE){
    if($product_id === FALSE){
        //$this->db->limit(8, 0);
        $query = $this->db->query("SELECT * FROM tb_stock");
        return $query->result_array();
    }
    //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
    $query  = $this->db->query("SELECT tb_other_product_stock.stock_id, tb_other_product_stock.stock_amount, tb_other_product_stock.stock_value, tb_other_product_stock.stock_desc, tb_other_product_stock.stock_created_at, tb_other_product_stock.stock_updated_at, tb_unit.unit_name, tb_location.location_name  FROM `tb_other_product_stock`, `tb_unit`, `tb_location` WHERE tb_unit.unit_id = tb_other_product_stock.unit_id AND tb_location.location_id = tb_other_product_stock.location_id AND product_id =".$product_id." order by tb_other_product_stock.stock_id desc");
    return $query->result_array();
}
  

  public function add_data(){
		$data = array(
      'stock_code' => $this->input->post('stock_code'),
      'stock_name' => $this->input->post('stock_name'),
      'stock_start' => $this->input->post('stock_start'),
      'stock_stop' => $this->input->post('stock_stop'),
      'stock_desc' => $this->input->post('stock_desc')
		);
		return $this->db->insert('tb_stock',$data);
  }

  public function other_product_add_data($data2){
		$data = array(
      'product_id' => $this->input->post('product_id'),
      'location_id' => $this->input->post('location_id'),
      'stock_amount' => $this->input->post('stock_amount'),
      'unit_id' => $this->input->post('unit_id'),
      'stock_desc' => $this->input->post('stock_desc')
    );
    $query  = $this->db->query("UPDATE tb_other_product SET product_global_stock=".$data2['total_stock']." WHERE tb_other_product.product_id=".$this->input->post('product_id'));
		return $this->db->insert('tb_other_product_stock',$data);
  }
  
  public function update_data()
  {
    $stock_id = $this->input->post('stock_id');
    $stock_code = $this->input->post('stock_code');
    $stock_name = $this->input->post('stock_name');
    $stock_start = $this->input->post('stock_start');
    $stock_stop = $this->input->post('stock_stop');
    $stock_desc = $this->input->post('stock_desc');

    $data  = array(
      'stock_code'          => $stock_code,
      'stock_name'         => $stock_name,
      'stock_start'           => $stock_start,
      'stock_stop'          => $stock_stop,
      'stock_desc'           => $stock_desc
    );
   $this->db->where('stock_id', $stock_id);
   return $this->db->update('tb_stock', $data);
  }

  //mengecek productname dengan num_row
  public function get_num_product_rows($data){
    $email = $data['product_email'];
    $password = md5($data['product_password']);

    $query = $this->db->query("SELECT * FROM tb_product where product_email = '$email' and product_password = '$password'");
    return $query->num_rows();
  }

  public function delete_data($id){
		$this->db->where('stock_id', $id);
		return $this->db->delete('tb_stock');
	}

}
