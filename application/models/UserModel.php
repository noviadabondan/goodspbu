<?php
class UserModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_user($user_id = FALSE){
        if($user_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_user");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_user',array('user_id' => $user_id));
        $query = $query = $this->db->query("SELECT * FROM tb_user WHERE user_id =".$user_id);
        return $query->row_array();
  }

  public function add_user($add_data){
    $user_profile_pict = $add_data['file_name'];
    $password = md5($this->input->post('user_password'));

    //changing date format to phpmyadmin
    $orgDate = $this->input->post('user_dob');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $dob = $newDate;

		$data = array(
			'user_name' => $this->input->post('user_name'),
			'user_profile_pict' => $user_profile_pict,
      'user_email' => $this->input->post('user_email'),
      'user_password' => $password,
      'user_status' => $this->input->post('user_status'),
      'user_dob' => $dob,
      'user_desc' => $this->input->post('user_desc')
		);
		return $this->db->insert('tb_user',$data);
  }
  
  public function update_user($additional_data = FALSE)
  {
    $user_id = $this->input->post('user_id');
    $user_name = $this->input->post('user_name');
    $user_email = $this->input->post('user_email');
    $user_status = $this->input->post('user_status');

    //changing date format to phpmyadmin database
    $orgDate = $this->input->post('user_dob');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $user_dob = $newDate;
    $user_desc = $this->input->post('user_desc');
    
   if(isset($additional_data['file_name'])){
     $data  = array(
       'user_name'          => $user_name,
       'user_email'         => $user_email,
       'user_profile_pict'  => $additional_data['file_name'],
       'user_dob'           => $user_dob,
       'user_desc'          => $user_desc,
       'user_status'        => $user_status
    );
  } else {
    $data  = array(
        'user_name'          => $user_name,
       'user_email'         => $user_email,
       'user_dob'           => $user_dob,
       'user_desc'          => $user_desc,
       'user_status'        => $user_status
   );
  }
   $this->db->where('user_id', $user_id);
   return $this->db->update('tb_user', $data);
  }

  public function user_delete($id){
    $this->db->where('user_id', $id);
    return $this->db->delete('tb_user');
  }


  //mengecek username dengan num_row
  public function get_num_user_rows($data){
    $email = $data['user_email'];
    $password = md5($data['user_password']);

    $query = $this->db->query("SELECT * FROM tb_user where user_email = '$email' and user_password = '$password'");
    return $query->num_rows();
  }

}
