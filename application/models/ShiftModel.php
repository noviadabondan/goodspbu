<?php
class ShiftModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_data($shift_id = FALSE){
        if($shift_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_shift");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_shift WHERE shift_id =".$shift_id);
        return $query->row_array();
  }

  public function add_data(){
		$data = array(
      'shift_code' => $this->input->post('shift_code'),
      'shift_name' => $this->input->post('shift_name'),
      'shift_start' => $this->input->post('shift_start'),
      'shift_stop' => $this->input->post('shift_stop'),
      'shift_desc' => $this->input->post('shift_desc')
		);
		return $this->db->insert('tb_shift',$data);
  }
  
  public function update_data()
  {
    $shift_id = $this->input->post('shift_id');
    $shift_code = $this->input->post('shift_code');
    $shift_name = $this->input->post('shift_name');
    $shift_start = $this->input->post('shift_start');
    $shift_stop = $this->input->post('shift_stop');
    $shift_desc = $this->input->post('shift_desc');

    $data  = array(
      'shift_code'          => $shift_code,
      'shift_name'         => $shift_name,
      'shift_start'           => $shift_start,
      'shift_stop'          => $shift_stop,
      'shift_desc'           => $shift_desc
    );
   $this->db->where('shift_id', $shift_id);
   return $this->db->update('tb_shift', $data);
  }

  //mengecek productname dengan num_row
  public function get_num_product_rows($data){
    $email = $data['product_email'];
    $password = md5($data['product_password']);

    $query = $this->db->query("SELECT * FROM tb_product where product_email = '$email' and product_password = '$password'");
    return $query->num_rows();
  }

  public function delete_data($id){
		$this->db->where('shift_id', $id);
		return $this->db->delete('tb_shift');
	}

}
