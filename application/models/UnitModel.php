<?php
class UnitModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_data($unit_id = FALSE){
        if($unit_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_unit");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_unit WHERE unit_id =".$unit_id);
        return $query->row_array();
  }

  public function get_unites_for_totalisator(){
    $query = $this->db->query("SELECT * FROM tb_unit WHERE unit_for='totalisator'");
    return $query->result_array();
  }

  public function get_unites_for_sale(){
    $query = $this->db->query("SELECT * FROM tb_unit WHERE unit_for='sale'");
    return $query->result_array();
  }

  public function get_unites_for_price(){
    $query = $this->db->query("SELECT * FROM tb_unit WHERE unit_for='price'");
    return $query->result_array();
  }

  public function add_data(){
		$data = array(
      'unit_name' => $this->input->post('unit_name'),
      'unit_value' => $this->input->post('unit_value'),
			'unit_desc' => $this->input->post('unit_desc')
		);
		return $this->db->insert('tb_unit',$data);
  }

  public function update_data($additional_data = FALSE)
  {
    $data = array(
      'unit_name' => $this->input->post('unit_name'),
      'unit_value' => $this->input->post('unit_value'),
			'unit_desc' => $this->input->post('unit_desc')
		);
    $this->db->where('unit_id', $this->input->post('unit_id'));
    return $this->db->update('tb_unit', $data);
  }

  public function data_delete($id){
		$this->db->where('unit_id', $id);
		return $this->db->delete('tb_unit');
	}
}
