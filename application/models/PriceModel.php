<?php
class PriceModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_data($price_id = FALSE){
        if($price_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_price ORDER BY price_id DESC");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_price WHERE price_id =".$price_id);
        return $query->row_array();
  }

  public function get_active_price($product_id){
    $query = $this->db->query("SELECT * FROM tb_price WHERE product_id=".$product_id." ORDER BY price_id DESC");
    return $query->row_array();
  }

  public function get_active_price_by_date($product_id,$date){
    $query = $this->db->query("SELECT * FROM tb_price WHERE product_id=".$product_id." AND price_date='".$date."'");
    return $query->row_array();
  }
  

  public function get_price_by_date_and_product_id($date,$product_id){
    $query = $this->db->query("SELECT * FROM tb_price WHERE price_date='$date' AND product_id='$product_id' ORDER BY price_id DESC");
    return $query->row_array();
  }

  public function add_data(){
    $price_difference = $this->input->post('price_sell') - $this->input->post('price_buy');
    //changing date format to phpmyadmin
    $orgDate = $this->input->post('price_date');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $price_date = $newDate;
		$data = array(
      'price_date' => $price_date,
      'price_code' => $this->input->post('price_code'),
      'price_buy' => $this->input->post('price_buy'),
      'price_sell' => $this->input->post('price_sell'),
			'price_difference' => $price_difference,
      'product_id' => $this->input->post('product_id'),
      'status_id' => $this->input->post('status_id'),
      'price_desc' => $this->input->post('price_desc')
		);
		return $this->db->insert('tb_price',$data);
  }

  public function add_data_by_product($add_data){
		$data = array(
      'price_date' => $add_data['price_date'],
      'price_code' => $add_data['price_code'],
      'price_buy' => $add_data['price_buy'],
      'price_sell' => $add_data['price_sell'],
			'price_difference' => $add_data['price_difference'],
      'product_id' => $add_data['product_id'],
      'status_id' => $add_data['status_id'],
      'price_desc' => $add_data['price_desc']
		);
		return $this->db->insert('tb_price',$data);
  }

  public function update_data($additional_data = FALSE)
  {
    $price_difference = $this->input->post('price_sell') - $this->input->post('price_buy');
    //changing date format to phpmyadmin
    $orgDate = $this->input->post('price_date');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $price_date = $newDate;
    $data = array(
      'price_date' => $price_date,
      'price_code' => $this->input->post('price_code'),
      'price_buy' => $this->input->post('price_buy'),
      'price_sell' => $this->input->post('price_sell'),
			'price_difference' => $price_difference,
      'product_id' => $this->input->post('product_id'),
      'status_id' => $this->input->post('status_id'),
      'price_desc' => $this->input->post('price_desc')
		);
    $this->db->where('price_id', $this->input->post('price_id'));
    return $this->db->update('tb_price', $data);
  }

  public function delete_data($id){
		$this->db->where('price_id', $id);
		return $this->db->delete('tb_price');
	}
}
