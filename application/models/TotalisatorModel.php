<?php
class TotalisatorModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_totalisator($totalisator_id = FALSE){
        if($totalisator_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_totalisator");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_totalisator WHERE totalisator_id =".$totalisator_id);
        return $query->row_array();
  }

public function get_totalisator_by_product($product_id){
    //$this->db->limit(8, 0);
    $query = $this->db->query("SELECT * FROM tb_totalisator WHERE product_id =".$product_id);
    return $query->result_array();
}

  public function add_data(){
		$data = array(
      'totalisator_code' => $this->input->post('totalisator_code'),
      'totalisator_name' => $this->input->post('totalisator_name'),
      'product_id' => $this->input->post('product_id'),
      'totalisator_current_position' => $this->input->post('totalisator_current_position'),
      'status_id' => $this->input->post('status_id'),
      'totalisator_desc' => $this->input->post('totalisator_desc')
		);
		return $this->db->insert('tb_totalisator',$data);
  }
  
  public function update_data()
  {
    $totalisator_id = $this->input->post('totalisator_id');
    $totalisator_code = $this->input->post('totalisator_code');
    $totalisator_name = $this->input->post('totalisator_name');
    $product_id = $this->input->post('product_id');
    $totalisator_current_position = $this->input->post('totalisator_current_position');
    $status_id = $this->input->post('status_id');
    $totalisator_desc = $this->input->post('totalisator_desc');

    $data  = array(
      'totalisator_code'          => $totalisator_code,
      'totalisator_name'         => $totalisator_name,
      'product_id'           => $product_id,
      'totalisator_current_position'          => $totalisator_current_position,
      'status_id'          => $status_id,
      'totalisator_desc'           => $totalisator_desc
    );
   $this->db->where('totalisator_id', $totalisator_id);
   return $this->db->update('tb_totalisator', $data);
  }

  public function product_delete($id){
    $this->db->where('product_id', $id);
    return $this->db->delete('tb_product');
  }


  //mengecek productname dengan num_row
  public function get_num_product_rows($data){
    $email = $data['product_email'];
    $password = md5($data['product_password']);

    $query = $this->db->query("SELECT * FROM tb_product where product_email = '$email' and product_password = '$password'");
    return $query->num_rows();
  }

  public function delete_data($id){
		$this->db->where('totalisator_id', $id);
		return $this->db->delete('tb_totalisator');
	}

}
