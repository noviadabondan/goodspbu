<?php
class SaleModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_data($sale_id = FALSE){
        if($sale_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_sale");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_sale WHERE sale_id =".$sale_id);
        return $query->row_array();
  }
  

  public function get_other_product_data($product_id = FALSE){
    if($product_id === FALSE){
        //$this->db->limit(8, 0);
        $query = $this->db->query("SELECT tb_other_product_sale.sale_id, tb_other_product_sale.sale_qty, tb_other_product_sale.sale_price, tb_other_product_sale.location_id, tb_other_product_sale.sale_desc, tb_other_product_sale.sale_created_at, tb_other_product_sale.sale_updated_at, tb_other_product.product_name, tb_other_product.product_price_sell, tb_location.location_name FROM tb_other_product_sale, tb_location, tb_other_product WHERE tb_other_product.product_id = tb_other_product_sale.product_id AND tb_location.location_id = tb_other_product_sale.location_id");
        return $query->result_array();
    }
    //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
    $query = $query = $this->db->query("SELECT tb_other_product_sale.sale_id, tb_other_product_sale.sale_qty, tb_other_product_sale.sale_price, tb_other_product_sale.location_id, tb_other_product_sale.sale_desc, tb_other_product_sale.sale_created_at, tb_other_product_sale.sale_updated_at, tb_other_product.product_name, tb_other_product.product_price_sell, tb_location.location_name FROM tb_other_product_sale, tb_location, tb_other_product WHERE tb_other_product.product_id = tb_other_product_sale.product_id AND tb_location.location_id = tb_other_product_sale.location_id AND tb_other_product_sale.product_id =".$product_id);
    return $query->row_array();
}

public function get_data_by_date($data){
    $orgDate = $data['report_start_date'];
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $date_start = $newDate;
    $orgDate = $data['report_end_date'];
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $date_end = $newDate;
    $query = $this->db->query("SELECT * FROM tb_sale WHERE sale_date BETWEEN '$date_start' AND '$date_end'");
    return $query->result_array();
}

public function get_data_existence_by_date($date){
  $orgDate = $date;
  $newDate = date("Y-m-d", strtotime($orgDate));  
  $date = $newDate;
  $query = $this->db->query("SELECT * FROM tb_sale WHERE sale_date = '$date'");
  return $query->num_rows();
}

public function get_data_existence_by_date_shift_and_totalisator($date, $shift, $totalisator_id){
  $orgDate = $date;
  $newDate = date("Y-m-d", strtotime($orgDate));  
  $date = $newDate;
  $query = $this->db->query("SELECT * FROM tb_sale WHERE sale_date = '$date' AND shift_id = '$shift' AND totalisator_id='$totalisator_id'");
  return $query->num_rows();
}

public function get_sale_by_date_shift_totalisator($date,$shift_id,$totalisator_id){
  $query = $this->db->query("SELECT * FROM tb_sale WHERE sale_date = '$date' AND shift_id = '$shift_id' AND totalisator_id = '$totalisator_id'");
  return $query->row_array();
}

  public function add_data(){
    $totalisator_difference = round($this->input->post('totalisator_position_after') - $this->input->post('totalisator_position_before'),3);
    $sale_difference = $this->input->post('sale_sell_price') - $this->input->post('sale_buy_price');
    //changing date format to phpmyadmin
    $orgDate = $this->input->post('sale_date');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $sale_date = $newDate;
		$data = array(
      'sale_date' => $sale_date,
      'shift_id' => $this->input->post('shift_id'),
      'totalisator_id' => $this->input->post('totalisator_id'),
      'product_id' => $this->input->post('product_id'),
      'user_email' => $this->input->post('user_email'),
      'status_id' => $this->input->post('status_id'),
      'totalisator_position_before' => $this->input->post('totalisator_position_before'),
      'totalisator_position_after' => $this->input->post('totalisator_position_after'),
      'totalisator_difference' => $totalisator_difference,
      'sale_buy_price' => $this->input->post('sale_buy_price'),
      'sale_sell_price' => $this->input->post('sale_sell_price'),
      'sale_difference' => $sale_difference,
      'sale_desc' => $this->input->post('sale_desc')
		);
		return $this->db->insert('tb_sale',$data);
  }

  public function add_data_by_date_and_shift($add_data){
    $orgDate = $add_data['sale_date'];
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $sale_date = $newDate;
		$data = array(
      'sale_date' => $sale_date,
      'shift_id' => $add_data['shift_id'],
      'totalisator_id' => $add_data['totalisator_id'],
      'product_id' => $add_data['product_id'],
      'user_email' => $add_data['user_email'],
      'status_id' => $add_data['status_id'],
      'totalisator_position_before' => $add_data['totalisator_position_before'],
      'totalisator_position_after' => $add_data['totalisator_position_after'],
      'totalisator_difference' => $add_data['totalisator_difference'],
      'sale_buy_price' => $add_data['sale_buy_price'],
      'sale_sell_price' => $add_data['sale_sell_price'],
      'sale_difference' => $add_data['sale_difference'],
      'sale_desc' => $add_data['sale_desc']
    );
    return $this->db->insert('tb_sale',$data);
  }

  public function update_data_by_date_and_shift($add_data){
    $orgDate = $add_data['sale_date'];
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $sale_date = $newDate;
		$data = array(
      'sale_date' => $sale_date,
      'shift_id' => $add_data['shift_id'],
      'totalisator_id' => $add_data['totalisator_id'],
      'product_id' => $add_data['product_id'],
      'user_email' => $add_data['user_email'],
      'status_id' => $add_data['status_id'],
      'totalisator_position_before' => $add_data['totalisator_position_before'],
      'totalisator_position_after' => $add_data['totalisator_position_after'],
      'totalisator_difference' => $add_data['totalisator_difference'],
      'sale_buy_price' => $add_data['sale_buy_price'],
      'sale_sell_price' => $add_data['sale_sell_price'],
      'sale_difference' => $add_data['sale_difference'],
      'sale_desc' => $add_data['sale_desc']
    );
    $this->db->where('sale_date', $sale_date);
    $this->db->where('shift_id', $add_data['shift_id']);
    $this->db->where('totalisator_id', $add_data['totalisator_id']);
    return $this->db->update('tb_sale', $data);
  }
  
  public function update_data()
  {
    $totalisator_difference = round($this->input->post('totalisator_position_after') - $this->input->post('totalisator_position_before'),3);
    $sale_difference = $this->input->post('sale_sell_price') - $this->input->post('sale_buy_price');
    //changing date format to phpmyadmin
    $sale_id = $this->input->post('sale_id');
    $orgDate = $this->input->post('sale_date');
    $newDate = date("Y-m-d", strtotime($orgDate));  
    $sale_date = $newDate;
		$data = array(
      'sale_date' => $sale_date,
      'shift_id' => $this->input->post('shift_id'),
      'totalisator_id' => $this->input->post('totalisator_id'),
      'product_id' => $this->input->post('product_id'),
      'user_email' => $this->input->post('user_email'),
      'status_id' => $this->input->post('status_id'),
      'totalisator_position_before' => $this->input->post('totalisator_position_before'),
      'totalisator_position_after' => $this->input->post('totalisator_position_after'),
      'totalisator_difference' => $totalisator_difference,
      'sale_buy_price' => $this->input->post('sale_buy_price'),
      'sale_sell_price' => $this->input->post('sale_sell_price'),
      'sale_difference' => $sale_difference,
      'sale_desc' => $this->input->post('sale_desc')
		);
   $this->db->where('sale_id', $sale_id);
   return $this->db->update('tb_sale', $data);
  }

  //mengecek productname dengan num_row
  public function get_num_product_rows($data){
    $email = $data['product_email'];
    $password = md5($data['product_password']);

    $query = $this->db->query("SELECT * FROM tb_product where product_email = '$email' and product_password = '$password'");
    return $query->num_rows();
  }

  public function delete_data($id){
		$this->db->where('sale_id', $id);
		return $this->db->delete('tb_sale');
	}

}
