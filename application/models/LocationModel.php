<?php
class LocationModel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_data($location_id = FALSE){
        if($location_id === FALSE){
            //$this->db->limit(8, 0);
            $query = $this->db->query("SELECT * FROM tb_location");
            return $query->result_array();
        }
        //$query = $this->db->get_where('tb_product',array('product_id' => $product_id));
        $query = $query = $this->db->query("SELECT * FROM tb_location WHERE location_id =".$location_id);
        return $query->row_array();
  }

  public function get_locationes_for_totalisator(){
    $query = $this->db->query("SELECT * FROM tb_location WHERE location_for='totalisator'");
    return $query->result_array();
  }

  public function get_locationes_for_sale(){
    $query = $this->db->query("SELECT * FROM tb_location WHERE location_for='sale'");
    return $query->result_array();
  }

  public function get_locationes_for_price(){
    $query = $this->db->query("SELECT * FROM tb_location WHERE location_for='price'");
    return $query->result_array();
  }

  public function add_data(){
		$data = array(
      'location_name' => $this->input->post('location_name'),
      'location_value' => $this->input->post('location_value'),
      'location_long' => $this->input->post('location_long'),
      'location_lat' => $this->input->post('location_lat'),
			'location_desc' => $this->input->post('location_desc')
		);
		return $this->db->insert('tb_location',$data);
  }

  public function update_data($additional_data = FALSE)
  {
    $data = array(
      'location_name' => $this->input->post('location_name'),
      'location_value' => $this->input->post('location_value'),
      'location_long' => $this->input->post('location_long'),
      'location_lat' => $this->input->post('location_lat'),
			'location_desc' => $this->input->post('location_desc')
		);
    $this->db->where('location_id', $this->input->post('location_id'));
    return $this->db->update('tb_location', $data);
  }

  public function data_delete($id){
		$this->db->where('location_id', $id);
		return $this->db->delete('tb_location');
	}
}
