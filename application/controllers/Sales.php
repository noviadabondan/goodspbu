<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {
	public function index()
	{
        $data['menu'] = "operation";
        $data['sales'] = $this->saleModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('sale');
		$this->load->view('templates/footer');
	}

	public function other_product()
	{
		$data['menu'] = "operation";
		$data['sales'] = $this->saleModel->get_other_product_data();
		$this->load->view('templates/header',$data);
		$this->load->view('sales-other-product');
		$this->load->view('templates/footer');
	}
	
    public function add()
	{
		$data['menu'] = "master";
		$data['statuses'] = $this->statusModel->get_statuses_for_sale();
        $data['products'] = $this->productModel->get_product();
        $data['shifts'] = $this->shiftModel->get_data();
        $data['user_email'] = $this->session->userdata('email');
        $data['totalisators'] = $this->totalisatorModel->get_totalisator();
		$this->load->view('templates/header',$data);
		$this->load->view('sale-add');
		$this->load->view('templates/footer');
	}

	public function other_product_sales_add()
	{
		$data['menu'] = "master";
		$data['statuses'] = $this->statusModel->get_statuses_for_sale();
        $data['products'] = $this->productModel->get_other_product();
        $data['shifts'] = $this->shiftModel->get_data();
        $data['user_email'] = $this->session->userdata('email');
        $data['totalisators'] = $this->totalisatorModel->get_totalisator();
		$this->load->view('templates/header',$data);
		$this->load->view('sales-other-product-add');
		$this->load->view('templates/footer');
	}

	public function add_simple_way()
	{
		$data['menu'] = "master";
		$data['statuses'] = $this->statusModel->get_statuses_for_sale();
        $data['products'] = $this->productModel->get_product();
        $data['shifts'] = $this->shiftModel->get_data();
        $data['user_email'] = $this->session->userdata('email');
		$data['totalisators'] = $this->totalisatorModel->get_totalisator();
		$this->load->view('templates/header',$data);
		$this->load->view('sale-add-simple-way');
		$this->load->view('templates/footer');
	}

	public function add_sale_by_date_and_shift()
	{
		$data['menu'] = "master";
		$data['statuses'] = $this->statusModel->get_statuses_for_sale();
        $data['products'] = $this->productModel->get_product();
        $data['shifts'] = $this->shiftModel->get_data();
        $data['user_email'] = $this->session->userdata('email');
		$data['totalisators'] = $this->totalisatorModel->get_totalisator();
		$this->load->view('templates/header',$data);
		$this->load->view('sale-add-by-date-and-shift');
		$this->load->view('templates/footer');
	}

	public function add_process()
	{
		$this->saleModel->add_data();
		redirect('sales');
	}

	public function get_sale_by_date_shift_totalisator($date,$shift_id,$totalisator_id)
	{
		$data['sale'] = $this->saleModel->get_sale_by_date_shift_totalisator($date,$shift_id,$totalisator_id);
		//echo $date.'-'.$shift_id.'-'.$totalisator_id;
		if($data['sale'] == null){
			echo "0";
		} else {
			echo json_encode($data['sale']);
		}
		
	}

	public function get_data_existence_by_date($date)
	{
		$data['existence'] = $this->saleModel->get_data_existence_by_date($date);
		echo $data['existence'];
	}


	public function add_process_by_date_and_shift()
	{
		$data['totalisators'] = $this->totalisatorModel->get_totalisator();
		
		foreach ($data['totalisators'] as $totalisator) : 
			$user_email = $this->input->post('user_email');
			$data['user_email'] = $user_email;
			$status_id = $this->input->post('status_id');
			$data['status_id'] = $status_id;
			$sale_desc = $this->input->post('sale_desc');
			$data['sale_desc'] = $sale_desc;
			$product_id = $totalisator['product_id'];
			$data['product_id'] = $product_id;
			$product_id = $totalisator['product_id'];
			$data['product_id'] = $product_id;
			$totalisator_id = $totalisator['totalisator_id'];
			$data['totalisator_id'] = $totalisator_id;
			$shift_id = $this->input->post('shift_id');
			$data['shift_id'] = $shift_id;
			$sale_date = $this->input->post('sale_date');
			$data['sale_date'] = $sale_date;
			$totalisator_position_before = $this->input->post('totalisator_before_'.$totalisator['totalisator_id']);
			$data['totalisator_position_before'] = $totalisator_position_before;
			$totalisator_position_after = $this->input->post('totalisator_after_'.$totalisator['totalisator_id']);
			$data['totalisator_position_after'] = $totalisator_position_after;
			$orgDate = $sale_date;
			$newDate = date("Y-m-d", strtotime($orgDate));  
			$active_price = $this->priceModel->get_active_price_by_date($product_id,$newDate);
			if($active_price==null){
			} else {
				$sale_buy_price = $active_price['price_buy'];
				$data['sale_buy_price'] = $sale_buy_price;
				$sale_sell_price = $active_price['price_sell'];
				$data['sale_sell_price'] = $sale_sell_price;
				$sale_difference = $sale_sell_price - $sale_buy_price;
				$data['sale_difference'] = $sale_difference;
			}
			
			
			if(($totalisator_position_before != null) && ($totalisator_position_after != null)){
				$totalisator_difference = round($totalisator_position_after - $totalisator_position_before,3);
				$data['totalisator_difference'] = $totalisator_difference;
			} else {
				$totalisator_before = 0;
				$totalisator_after = 0;
				$totalisator_difference = 0;
				$data['totalisator_difference'] = $totalisator_difference;
			}
			// echo 'buy price: '.$sale_buy_price;
			// echo '<br>';
			// echo 'sell price: '.$sale_sell_price;
			// echo '<br>';
			// echo 'price difference: '.$sale_difference;
			// echo '<br>';
			// echo 'sale date: '.$sale_date;
			// echo '<br>';
			// echo 'user_email: '.$user_email;
			// echo '<br>';
			// echo 'shift id.: '.$shift_id;
			// echo '<br>';
			// echo 'Product ID: '.$product_id;
			// echo '<br>';
			// echo 'Totalisator ID: '.$totalisator_id;
			// echo '<br>';
			// echo 'totalisator_before_'.$totalisator['totalisator_id'].': '.$totalisator_position_before;
			// echo '<br>';
			// echo 'totalisator_after_'.$totalisator['totalisator_id'].': '.$totalisator_position_after;
			// echo '<br>';
			// echo 'totalisator_difference_'.$totalisator['totalisator_id'].': '. $totalisator_difference;
			// echo '<br>';
			// echo 'status id: '.$status_id;
			// echo '<br>';
			// echo 'sale note: '.$sale_desc;
			// echo '<br>';
			// echo '<br>';
			if($active_price==null){
				//echo "Please update the price for today :)";
			} else {
				$data['existence'] = $this->saleModel->get_data_existence_by_date_shift_and_totalisator($this->input->post('sale_date'), $this->input->post('shift_id'), $totalisator['totalisator_id']);
				if($data['existence'] == 0) {
					// $i=1;
					// for($i=1;$i==1000;$i++){
					// 	$i=$i+1;
					// 	$this->saleModel->add_data_by_date_and_shift($data);
					// }
					$this->saleModel->add_data_by_date_and_shift($data);
					//echo $data['existence']." kosong";
				} else {
					$this->saleModel->update_data_by_date_and_shift($data);
					//echo $data['existence']." isi";
				}
			}
		endforeach;
		if($active_price==null){
			echo 'Please update the price for today :) <a href="'.base_url().'/price/add_price_by_product">click here</a>';
		} else {
			redirect('sales');
		}
		
	}

	public function edit($sale_id)
	{
		$data['menu'] = "operation";
		$data['user_email'] = $this->session->userdata('email');
		$data['sale'] = $this->saleModel->get_data($sale_id);
		$data['shifts'] = $this->shiftModel->get_data();
		$data['shift'] = $this->shiftModel->get_data($data['sale']['shift_id']);
		$data['shift_name'] = $data['shift']['shift_name'];
		$data['totalisators'] = $this->totalisatorModel->get_totalisator();
		$data['totalisator'] = $this->totalisatorModel->get_totalisator($data['sale']['totalisator_id']);
		$data['totalisator_name'] = $data['totalisator']['totalisator_name'];
		$data['statuses'] = $this->statusModel->get_statuses_for_sale();
		$data['status'] = $this->statusModel->get_statuses($data['sale']['status_id']);
		$data['status_name'] = $data['status']['status_name'];
		$data['products'] = $this->productModel->get_product();
		$data['product'] = $this->productModel->get_product($data['sale']['product_id']);
		$data['product_name'] = $data['product']['product_name'];
		$this->load->view('templates/header',$data);
		$this->load->view('sale-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		$this->saleModel->update_data();
		redirect('sales');
	}

	public function delete($id)
	{	
		$this->saleModel->delete_data($id);
		redirect('sales');
	}

}
