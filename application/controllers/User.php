<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {


	public function index()
	{
		$data['menu'] = "master";
		$data['users'] = $this->userModel->get_user();
		$this->load->view('templates/header',$data);
		$this->load->view('user');
		$this->load->view('templates/footer');
    }

    public function get_user_detail($user_id)
	{
		$data['menu'] = "master";
		$data['user'] = $this->userModel->get_user($user_id);
		$this->load->view('user-detail',$data);
	}

	public function get_username($user_id)
	{
		$data['user'] = $this->userModel->get_user($user_id);
		echo $data['user']['user_name'];
	}

	public function add()
	{
		$data['menu'] = "master";
		$this->load->view('templates/header',$data);
		$this->load->view('user-add');
		$this->load->view('templates/footer');
	}

	public function add_process()
	{
		$config['upload_path']          = './assets/img/user';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Add a User';
		if(!$this->upload->do_upload('user_profile_pict')){
			$data['file_name']= "kosong.jpg";
			$this->userModel->add_user($data);
			redirect('user');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->userModel->add_user($data);
			redirect('user');
		}
	}

	public function edit($user_id)
	{
		$data['menu'] = "master";
		$data['user'] = $this->userModel->get_user($user_id);
		$this->load->view('templates/header',$data);
		$this->load->view('user-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		$config['upload_path']          = './assets/img/user';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		if(!$this->upload->do_upload('user_profile_pict')){
			//$data['file_name']= "kosong.jpg"; harus return null
			$this->userModel->update_user($data);
			redirect('user');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->userModel->update_user($data);
			redirect('user');
		}
	}

	public function delete($id)
	{	
		//menghapus file gambar
		$data_row = $this->userModel->get_user($id);
		$file_name = $data_row['user_profile_pict'];
		//echo $file_name;
		unlink('assets/img/user/'.$file_name);


		$this->userModel->user_delete($id);
			redirect('user');
		}

	
}
