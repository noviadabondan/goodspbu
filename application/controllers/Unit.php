<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends CI_Controller {
	public function index()
	{
        $data['menu'] = "master";
        $data['unites'] = $this->unitModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('unit');
		$this->load->view('templates/footer');
	}

	public function get_unit_name($unit_id)
	{
		$data['unit'] = $this->unitModel->get_data($unit_id);
		echo $data['unit']['unit_name'];
	}
	
	public function add()
	{
		$data['menu'] = "master";
		$this->load->view('templates/header',$data);
		$this->load->view('unit-add');
		$this->load->view('templates/footer');
	}

	public function add_process()
	{
		$this->unitModel->add_data();
		redirect('unit');
	}

	public function edit($unit_id)
	{
		$data['menu'] = "master";
		$data['unit'] = $this->unitModel->get_data($unit_id);
		$this->load->view('templates/header',$data);
		$this->load->view('unit-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		
		$this->unitModel->update_data();
		redirect('unit');
		
	}

	public function delete($id)
	{	
		$this->unitModel->data_delete($id);
		redirect('unit');
	}

	
}
