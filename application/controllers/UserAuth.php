<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserAuth extends CI_Controller {
	public function index()
	{
		$this->load->view('login');
	}
    public function login_process()
	{
        $this->form_validation->set_rules('user_email', 'email', 'required');
		$this->form_validation->set_rules('user_password', 'password', 'required');
		if($this->form_validation->run() === FALSE)
            {
                $data['title'] = "login";
                $this->load->view('login',$data);
            } else {
                $data['user_email'] = $this->input->post('user_email');
                $data['user_password'] = $this->input->post('user_password');
                $num_user_row = $this->userModel->get_num_user_rows($data);

                if($num_user_row==0){
                    $data['error'] = '
                    <div class="box-content bordered-all danger">
                        <h4 class="box-title"><i class="ico fa fa-warning"></i>Error</h4>
                        <!-- /.box-title -->
                        <!-- /.dropdown js__dropdown -->
                        <p>Username atau Password anda Salah!</p>
                    </div>';
                    $this->load->view('login',$data);
                } else {
                    //$this->post_model->set_article($data);
                    $email = $this->input->post('user_email');
                    $password = $this->input->post('user_password');
                    //cek database disini
                    $this->session->set_userdata(array(
                        'email'         => $email,
                        'password'      => $password,
                        'status'        => TRUE
                    ));
                    redirect('home');
                }
            }
	}


    public function logout($value='')
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('password');
        $this->session->unset_userdata('status');
        $this->session->sess_destroy();
        redirect('userAuth');
    }


}
