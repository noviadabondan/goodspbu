<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {
	public function index()
	{
        $data['menu'] = "master";
        $data['statuses'] = $this->statusModel->get_statuses();
		$this->load->view('templates/header',$data);
		$this->load->view('status');
		$this->load->view('templates/footer');
	}

	public function get_status_name($status_id)
	{
		$data['status'] = $this->statusModel->get_statuses($status_id);
		echo $data['status']['status_name'];
	}
	
	public function add()
	{
		$data['menu'] = "master";
		$this->load->view('templates/header',$data);
		$this->load->view('status-add');
		$this->load->view('templates/footer');
	}

	public function add_process()
	{
		$this->statusModel->add_status();
		redirect('status');
	}

	public function edit($status_id)
	{
		$data['menu'] = "master";
		$data['status'] = $this->statusModel->get_statuses($status_id);
		$this->load->view('templates/header',$data);
		$this->load->view('status-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		
		$this->statusModel->update_status();
		redirect('status');
		
	}

	public function delete($id)
	{	
		$this->statusModel->status_delete($id);
		redirect('status');
	}

	
}
