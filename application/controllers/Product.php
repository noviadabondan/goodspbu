<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	public function index()
	{
        $data['menu'] = "master";
        $data['products'] = $this->productModel->get_product();
		$this->load->view('templates/header',$data);
		$this->load->view('product');
		$this->load->view('templates/footer');
	}

	public function other_product()
	{
        $data['menu'] = "master";
        $data['products'] = $this->productModel->get_other_product();
		$this->load->view('templates/header',$data);
		$this->load->view('other-product');
		$this->load->view('templates/footer');
	}

	public function other_product_stock()
	{
        $data['menu'] = "master";
        $data['products'] = $this->productModel->get_other_product();
		$this->load->view('templates/header',$data);
		$this->load->view('other-product-stock');
		$this->load->view('templates/footer');
	}
	
    public function add()
	{
		$data['menu'] = "master";
		$this->load->view('templates/header',$data);
		$this->load->view('product-add');
		$this->load->view('templates/footer');
	}

	public function add_other_product()
	{
		$data['menu'] = "master";
		$data['units'] = $this->unitModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('other-product-add');
		$this->load->view('templates/footer');
	}
	
    public function add_process()
	{
		$config['upload_path']          = './assets/img/product';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Add a product';
		if(!$this->upload->do_upload('product_image')){
			$data['file_name']= "kosong.jpg";
			$this->productModel->add_product($data);
			redirect('product');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->productModel->add_product($data);
			redirect('product');
		}
	}

	public function other_product_add_process()
	{
		$config['upload_path']          = './assets/img/product';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Add an other product';
		if(!$this->upload->do_upload('product_image')){
			$data['file_name']= "kosong.jpg";
			$this->productModel->add_other_product($data);
			redirect('product/other_product');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->productModel->add_other_product($data);
			redirect('product/other_product');
		}
	}

	public function get_product_view($date,$shift_id)
	{
		if(isset($date)){
			$data['date'] = $date;
			$data['shift_id'] = $shift_id;
			$data['products'] = $this->productModel->get_product();
			$data['totalisators'] = $this->totalisatorModel->get_totalisator();
			$this->load->view('product-view',$data);
		} else {
			$data['products'] = $this->productModel->get_product();
			$data['totalisators'] = $this->totalisatorModel->get_totalisator();
			$this->load->view('product-view',$data);
		}
		
		
	}

	public function get_product_name($product_id)
	{
		$data['product'] = $this->productModel->get_product($product_id);
		echo $data['product']['product_name'];
	}

	public function get_product_detail($product_id)
	{
		$data['menu'] = "master";
		$data['product'] = $this->productModel->get_product($product_id);
		$data['price'] = $this->priceModel->get_active_price($product_id);
		//$data['price'] = $this->priceModel->get_price($product_id);
		$this->load->view('product-detail',$data);
	}

	public function get_other_product_price($product_id)
	{
		$data['product'] = $this->productModel->get_other_product($product_id);
		echo $data['product']['product_price_sell'];
	}

	public function get_other_product_detail($product_id)
	{
		$data['menu'] = "master";
		$data['product'] = $this->productModel->get_other_product($product_id);
		$data['price'] = $this->priceModel->get_active_price($product_id);
		$data['unit'] = $this->unitModel->get_data($data['product']['unit_id']);
		$data['unit_name'] = $data['unit']['unit_name'];
		//$data['price'] = $this->priceModel->get_price($product_id);
		$this->load->view('other-product-detail',$data);
	}

	public function edit($product_id)
	{
		$data['menu'] = "master";
		$data['product'] = $this->productModel->get_product($product_id);
		$this->load->view('templates/header',$data);
		$this->load->view('product-edit');
		$this->load->view('templates/footer');
	}

	public function edit_other_product($product_id)
	{
		$data['menu'] = "master";
		$data['product'] = $this->productModel->get_other_product($product_id);
		$data['unit'] = $this->unitModel->get_data($data['product']['unit_id']);
		$data['unit_name'] = $data['unit']['unit_name'];
		$data['units'] = $this->unitModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('other-product-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		$config['upload_path']          = './assets/img/product';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		if(!$this->upload->do_upload('product_image')){
			//$data['file_name']= "kosong.jpg"; harus return null
			$this->productModel->update_product($data);
			redirect('product');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->productModel->update_product($data);
			redirect('product');
		}
	}

	public function other_product_edit_process(){
		$config['upload_path']          = './assets/img/product';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		if(!$this->upload->do_upload('product_image')){
			//$data['file_name']= "kosong.jpg"; harus return null
			$this->productModel->update_other_product($data);
			redirect('product/other_product');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->productModel->update_other_product($data);
			redirect('product/other_product');
		}
	}

	public function delete($id)
	{	
		$this->productModel->product_delete($id);
		redirect('product');
	}

	public function other_product_delete($id)
	{	
		$this->productModel->other_product_delete($id);
		redirect('product/other_product');
	}
}
