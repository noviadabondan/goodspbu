<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	public function index()
	{
		$data['menu'] = "dashboard";
		$this->load->view('row-append');
	}

	public function advanced_dropdown_with_search()
	{
		$data['menu'] = "dashboard";
		$this->load->view('templates/header',$data);
		$this->load->view('advanced_dropdown_with_search');
		$this->load->view('templates/footer');
	}

	public function view_data(){
		$lineProduct = $this->input->post('lineProduct');
		//echo 'lineproduct : '.$lineProduct.'<br>';
		for ($x = 1; $x <= $lineProduct; $x++) {
			$product_id = $this->input->post('product_id'.$x);
			echo 'product : '.$product_id.'<br>';
			$price = $this->input->post('price'.$x);
			echo 'price : '.$price.'<br>';
		  }

	}

	public function get_data(){
		echo "5000";
	}
}
