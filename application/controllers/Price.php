<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price extends CI_Controller {
	public function index()
	{
        $data['menu'] = "operation";
        $data['prices'] = $this->priceModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('price');
		$this->load->view('templates/footer');
	}

	public function get_active_price($product_id)
	{
		$data =  $this->priceModel->get_active_price($product_id);
		echo json_encode($data);
	}

	public function get_active_price_by_date($product_id,$date)
	{
		$data =  $this->priceModel->get_active_price_by_date($product_id,$date);
		echo json_encode($data);
	}

	public function get_price($product_id)
	{
		$data['price'] =  $this->priceModel->get_active_price($product_id);
		echo $data['price']['price_sell'];
	}

	public function get_price_by_date_and_product_id($date,$product_id)
	{
		$data['price'] =  $this->priceModel->get_price_by_date_and_product_id($date,$product_id);
		if($data['price'] == null){
			echo "null";
		} else {
			echo json_encode($data['price']);
		}
		// echo $date."/".$product_id;
	}

	public function add()
	{
		$data['menu'] = "operation";
		$data['statuses'] = $this->statusModel->get_statuses_for_price();
		$data['products'] = $this->productModel->get_product();
		$this->load->view('templates/header',$data);
		$this->load->view('price-add');
		$this->load->view('templates/footer');
	}

	public function add_price_by_product()
	{
		$data['menu'] = "operation";
		$data['statuses'] = $this->statusModel->get_statuses_for_price();
		$data['products'] = $this->productModel->get_product();
		$this->load->view('templates/header',$data);
		$this->load->view('price-add-by-product');
		$this->load->view('templates/footer');
	}
	
	public function add_process()
	{
		$this->priceModel->add_data();
		redirect('price');
	}

	public function add_process_by_product()
	{
		$data['products'] = $this->productModel->get_product();
		foreach ($data['products'] as $product) : 
			$add_data['product_id']=$product['product_id'];
			$orgDate = $this->input->post('price_date');
			$newDate = date("Y-m-d", strtotime($orgDate));  
			$price_date = $newDate;
			$add_data['price_date']=$price_date;
			// echo $price_date;
			// echo '<br>';
			$price_code = $this->input->post('price_code');
			$add_data['price_code']=$price_code;
			// echo $price_code;
			// echo '<br>';
			$price_buy = $this->input->post('price_buy_'.$product['product_id']);
			$add_data['price_buy']=$price_buy;
			// echo $price_buy;
			// echo '<br>';
			$price_sell = $this->input->post('price_sell_'.$product['product_id']);
			$add_data['price_sell']=$price_sell;
			// echo $price_sell;
			// echo '<br>';
			$price_difference = $price_sell - $price_buy;
			$add_data['price_difference']=$price_difference;
			// echo $price_difference;
			// echo '<br>';
			$status_id = $this->input->post('status_id');
			$add_data['status_id']=$status_id;
			// echo $status_id;
			// echo '<br>';
			$price_desc = $this->input->post('price_desc');
			$add_data['price_desc']=$price_desc;
			// echo $price_desc;
			// echo '<br>';
			// echo '<br>';
			$this->priceModel->add_data_by_product($add_data);
		endforeach;
		redirect('price');
	}

	public function edit($price_id)
	{
		$data['menu'] = "operation";
		$data['price'] = $this->priceModel->get_data($price_id);
		$data['statuses'] = $this->statusModel->get_statuses_for_totalisator();
		$data['status'] = $this->statusModel->get_statuses($data['price']['status_id']);
		$data['status_name'] = $data['status']['status_name'];
		$data['products'] = $this->productModel->get_product();
		$data['product'] = $this->productModel->get_product($data['price']['product_id']);
		$data['product_name'] = $data['product']['product_name'];
		$this->load->view('templates/header',$data);
		$this->load->view('price-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		$this->priceModel->update_data();
		redirect('price');
	}

	public function delete($id)
	{	
		$this->priceModel->delete_data($id);
		redirect('price');
	}
}
