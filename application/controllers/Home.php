<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
		{
			redirect('userAuth');
		}

	}
	public function index()
	{
		$data['menu'] = "dashboard";
		$this->load->view('templates/header',$data);
		$this->load->view('home');
		$this->load->view('templates/footer');
	}

	public function home2()
	{
		$data['menu'] = "dashboard";
		$this->load->view('templates/header',$data);
		$this->load->view('home');
		$this->load->view('templates/footer');
	}
}
