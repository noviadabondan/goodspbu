<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shift extends CI_Controller {
	public function index()
	{
        $data['menu'] = "master";
        $data['shifts'] = $this->shiftModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('shift');
		$this->load->view('templates/footer');
	}

	public function get_shift_name($shift_id)
	{
		$data['shift'] = $this->shiftModel->get_data($shift_id);
		echo $data['shift']['shift_name'];
	}

	public function add()
	{
		$data['menu'] = "master";
		$this->load->view('templates/header',$data);
		$this->load->view('shift-add');
		$this->load->view('templates/footer');
	}
	
	public function add_process()
	{
		$this->shiftModel->add_data();
		redirect('shift');
	}

	public function edit($shift_id)
	{
		$data['menu'] = "master";
		$data['shift'] = $this->shiftModel->get_data($shift_id);
		$this->load->view('templates/header',$data);
		$this->load->view('shift-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		$this->shiftModel->update_data();
		redirect('shift');
	}

	public function delete($id)
	{	
		$this->shiftModel->delete_data($id);
		redirect('shift');
	}
}
