<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {
	public function index()
	{
        $data['menu'] = "master";
		$this->load->view('templates/header');
		$this->load->view('stock');
		$this->load->view('templates/footer');
	}

	public function other_product($product_id)
	{
		$data['menu'] = "master";
		$data['product_id'] = $product_id;
		$data['product'] = $this->productModel->get_other_product($product_id);
		$data['product_name'] = $data['product']['product_name'];
		$data['stocks'] = $this->stockModel->get_other_product_stock($product_id);
		$this->load->view('templates/header',$data);
		$this->load->view('stock_other_product');
		$this->load->view('templates/footer');
	}

	public function other_product_add($product_id)
	{
		$data['menu'] = "master";
		$data['product_id'] = $product_id;
		$data['product'] = $this->productModel->get_other_product($product_id);
		$data['unit_id'] = $data['product']['unit_id'];
		$data['product_current_stock'] = $data['product']['product_global_stock'];
		$data['locations'] = $this->locationModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('stock-other-product-add');
		$this->load->view('templates/footer');
	}

	public function other_product_add_process()
	{
		$product_id = $this->input->post('product_id');
		$data['product'] = $this->productModel->get_other_product($product_id);
		$stock_current = $data['product']['product_global_stock'];
		$stock_amount = $this->input->post('stock_amount');
		$total_stock = $stock_amount + $stock_current;
		// echo $total_stock;
		$data['total_stock'] = $total_stock;
		$this->stockModel->other_product_add_data($data);
		redirect('stock/other_product/'.$product_id);
	}
}
