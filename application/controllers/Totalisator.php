<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Totalisator extends CI_Controller {
	public function index()
	{
        $data['menu'] = "master";
        $data['totalisators'] = $this->totalisatorModel->get_totalisator();
		$this->load->view('templates/header',$data);
		$this->load->view('totalisator');
		$this->load->view('templates/footer');
	}

	public function get_totalisators()
	{
		$data['totalisators'] = $this->totalisatorModel->get_totalisator();
	}

	public function get_totalisator_name($totalisator_id)
	{
		$data['totalisator'] = $this->totalisatorModel->get_totalisator($totalisator_id);
		echo $data['totalisator']['totalisator_name'];
	}

	public function get_totalisator_by_product($product_id)
	{
		$data['totalisator'] = $this->totalisatorModel->get_totalisator_by_product($product_id);
		echo json_encode($data);
	}

	public function get_totalisator_input_by_product($product_id)
	{
		$data['totalisators'] = $this->totalisatorModel->get_totalisator_by_product($product_id);
		//$data['price'] = $this->priceModel->get_price($product_id);
		$this->load->view('totalisator-input-view',$data);
	}

	public function add()
	{
		$data['menu'] = "master";
		$data['statuses'] = $this->statusModel->get_statuses_for_totalisator();
		$data['products'] = $this->productModel->get_product();
		$this->load->view('templates/header',$data);
		$this->load->view('totalisator-add');
		$this->load->view('templates/footer');
	}
	
	public function add_process()
	{
		$this->totalisatorModel->add_data();
		redirect('totalisator');
	}

	public function edit($totalisator_id)
	{
		$data['menu'] = "master";
		$data['totalisator'] = $this->totalisatorModel->get_totalisator($totalisator_id);
		$data['statuses'] = $this->statusModel->get_statuses_for_totalisator();
		$data['status'] = $this->statusModel->get_statuses($data['totalisator']['status_id']);
		$data['status_name'] = $data['status']['status_name'];
		$data['products'] = $this->productModel->get_product();
		$data['product'] = $this->productModel->get_product($data['totalisator']['product_id']);
		$data['product_name'] = $data['product']['product_name'];
		$this->load->view('templates/header',$data);
		$this->load->view('totalisator-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		$this->totalisatorModel->update_data();
		redirect('totalisator');
	}

	public function delete($id)
	{	
		$this->totalisatorModel->delete_data($id);
		redirect('totalisator');
	}
}
