<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {
	public function index()
	{
        $data['menu'] = "master";
        $data['locations'] = $this->locationModel->get_data();
		$this->load->view('templates/header',$data);
		$this->load->view('location');
		$this->load->view('templates/footer');
	}

	public function get_location_name($location_id)
	{
		$data['location'] = $this->locationModel->get_locations($location_id);
		echo $data['location']['location_name'];
	}
	
	public function add()
	{
		$data['menu'] = "master";
		$this->load->view('templates/header',$data);
		$this->load->view('location-add');
		$this->load->view('templates/footer');
	}

	public function add_process()
	{
		$this->locationModel->add_data();
		redirect('location');
	}

	public function edit($location_id)
	{
		$data['menu'] = "master";
		$data['location'] = $this->locationModel->get_data($location_id);
		$this->load->view('templates/header',$data);
		$this->load->view('location-edit');
		$this->load->view('templates/footer');
	}

	public function edit_process(){
		
		$this->locationModel->update_data();
		redirect('location');
		
	}

	public function delete($id)
	{	
		$this->locationModel->data_delete($id);
		redirect('location');
	}

	
}
