<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
		{
			redirect('userAuth');
		}

	}
	public function index()
	{
		$data['menu'] = "report";
		$this->load->view('templates/header',$data);
		$this->load->view('home');
		$this->load->view('templates/footer');
    }
    
    public function sales_date_range()
	{
        $data['menu'] = "report";
        $this->load->view('templates/header',$data);
		$this->load->view('report-sales-date-range');
		$this->load->view('templates/footer');
    }

    public function sales()
	{
        $data['menu'] = "report";
        $data['totalisators'] = $this->totalisatorModel->get_totalisator();
        $data['products'] = $this->productModel->get_product();
        $data['shifts'] = $this->shiftModel->get_data();
        $data['report_start_date'] = $this->input->post('report_start_date');
        $data['report_end_date'] = $this->input->post('report_end_date');
        $orgDate = $data['report_start_date'];
        $newDate = date("Y-m-d", strtotime($orgDate));  
        $date_start = $newDate;
        $orgDate = $data['report_end_date'];
        $newDate = date("Y-m-d", strtotime($orgDate));  
        $date_end = $newDate;
        $data['sales'] = $this->saleModel->get_data_by_date($data);
        $this->load->view('report-sales',$data);
        
        // $begin = new DateTime($date_start);
        // $end = new DateTime($date_end);
        // $end = $end->modify( '+1 day' ); 

        // $interval = new DateInterval('P1D');
        // $daterange = new DatePeriod($begin, $interval ,$end);

        // foreach($daterange as $date){
        //     echo $date->format("Y-m-d") . "<br>";

        // }
	}
}
